![Developer cover Image](https://gitlab.com/Epitech-2017-2018/Unix-Network/PSU_zappy_2017_gitlab/raw/master/doxygen/ZappyDevImage.png)

- [Home page](https://gitlab.com/Epitech-2017-2018/Unix-Network/PSU_zappy_2017_gitlab/wikis/home)
- [Server](#server)
- [AI Client](#ai-client)
- [Graphical Client](#graphical-client)
- [Documentation](#documentation)
- [Unit tests](#unit-tests)
- [GitLab](#ourflow)
- [Coding style](#coding-style)
- [Languages](#languages)

</h2 id="server"></h2>
## Server
> This section describes technical aspects of the Zappy server.

### Connection management
The server uses simple tools to manage incoming connections. In fact, sockets are managed by the [Epoll Unix Library](http://man7.org/linux/man-pages/man7/epoll.7.html). This allows the server to listen / accept / write on sockets.

### Data management
Each socket corresponds to a client (in-game player). Therefore, each client sends commands to the server. A [ring buffer](https://en.wikipedia.org/wiki/Circular_buffer) is used to store the commands and prevent data loss over the network.

### Command management (players)
Each client (in-game) player has a command queue which can stack up to 10 commands. These commands require time to be executed, the server decrements time for the first received command, executes it and does the same for the command in the queue. Here is an exhaustive list of the available commands:

Command | Action | Time | Server Response
:-------|:------:|:----:|:---------------:
Forward | Move one tile forward | 7/f | ok
Right | Turn 90° right | 7/f | ok
Left | Turn 90° left | 7/f | ok
Look | Look around | 7/f | [tile1, tile2, ...]
Inventory | Inventory | 1/f | [linemate n, sibur n, ...]
Broadcast text | Send text throughout the map | 7/f | ok
Connect_nbr | Number of team unused slots | - | value
Fork | Player reproduction | 42/f - Egg hatching 600/f | ok
Eject | Eject players from the tile | 7/f | ok/ko
- | Player is dead | - | dead
Take object | Take an object on the tile | 7/f | ok/ko
Set object | Set an object on the tile | 7/f | ok/ko
Incantation | Player level up | 300/f | Elevation underway... Current level : *k* / ko

### Command management (graphical client)
The graphical client uses a different protocol from the normal players. In fact, the only command for the graphical client is PING. This command has a detailed response at the server sends all of the informations about the map and players. Here is a description of the graphical protocol:

* @GRAPHICAL - Username for the graphical client - `MUST BE USED ON STARTUP`

Response:

```bash
$ x (map width) y (map height) f (server frequence)
$ [team teamid | ...]
$ Example:
$ 10 10 100
$ BestTeam 1 | AnotherGreatTeam 2
```

* PING - Get server information

Response:

```bash
$ x;y(tile position)=team_id-player_id:direction/level/life/resource_quantity,resource_name:resource_quantity,another_resource:resource_quantity
$ Example:
$ 1;0=1-0:0/1/1260/food_10/linemate_0/deraumere_0/sibur_0/mendiane_0/phiras_0/thystame_0,food:4,linemate:0,deraumere:1,sibur:0,mendiane:0,phiras:1,thystame:0
```

### More informations

For more informations checkout the [Documentation](#documentation) section of this page.

</h2 id="ai-client"></h2>
## AI Client

</h2 id="graphical-client"></h2>
## Graphical Client

</h2 id="documentation"></h2>
## Documentation
> This section gives details about the documentation for the Zappy project.

Developer documentation for this project was generated with [Doxygen](http://www.doxygen.org) directly from the source code.

### Generation

To generate the documentation, at the root of the project execute the following command:

```bash
$ make documentation
```

It the generation fails, please contact us.

### Website

The generation of the documentation produces a complete website for the project. To view this website, the main page is located (from the root of the project folder) at it follows:

```bash
$ firefox ./doxygen/html/index.html
```

On a Linux distribution this command will start Firefox and open the home page of the doucmentation

#### If you're having issues generating or viewing the documentation please contact us.

</h2 id="unit-tests"></h2>
## Unit tests
> This section highlights how unit tests were made for this project.

Unit tests for this project were created and coded with the [Criterion](https://github.com/Snaipe/Criterion) Unit test library. Each global function in the project is tested on its own.

### Generation

To generate the unit tests, at the root of the project folder execute the following command:

```bash
$ make tests_run
```

### Execution

To launch the test, at the root of the project folder execute the following command:

```bash
$ ./tests_run
```

#### If you're having issues generating or executing the unit tests please contact us.

</h2 id="ourflow"></h2>
## GitLab
> This section describes why and how we use GitLab to host the project.

### Collaboration

GitLab allows for deep collaboration. Indeed, a role is assigned to each participant to the project.

### Branches

GitLab allows for branch organised projects. For each new functionnalty or feature to be added, a branch is created and merged on the master branch when fully functionnal and compatible with the other branches.

### Pipelines

GitLab allows users to create pipelines which are jobs executed on each commits. We use the Jenkins platform on a remote server to automatically launch the unit tests for each commit.

### Commits

Each commit made the project follows this simple commit style :

Command | Action
:-------|:------:
Add | New feature / functionnality / file(s)
Remove | Removed feature / functionnality / file(s)
Modify | Modified feature / functionnality / file(s)
Merge | Merge between branches

A commit message is composed of the command, the ':' separator and additional informations. Example:

```bash
$ Add: player command queue.
$ Remove: unnecesary files.
```

#### If you want more informations about our GitLab workflow, please contact us.

</h2 id="coding-style"></h2>
## Coding style
> This section describes the coding style used for the project.

### Unix Kernel C Coding Style

The C code present in this project is compliant with the [Unix Kernel Coding Style](https://www.kernel.org/doc/html/v4.10/process/coding-style.html).

### Python PEP8

The Python code present in this project is compliant with the [Python PEP 8 Coding Style](https://www.python.org/dev/peps/pep-0008/).

### Folders

- A folder corresponds to a set of tools / functions directly linked to one module / specific part of the program.
- The name of the folder begins with the name of the parent folder.


### Files

- A file can only contain one global function and four static functions.
- The name of the file is directly inherited form the name of the global function.
- Each file only includes the necessary modules.

</h2 id="languages"></h2>
## Languages
> This section gives a list of the used languages for the Zappy project.

Language repartition graph:

![Language chart](https://gitlab.com/Epitech-2017-2018/Unix-Network/PSU_zappy_2017_gitlab/raw/master/doxygen/LanguageRepartition.png)