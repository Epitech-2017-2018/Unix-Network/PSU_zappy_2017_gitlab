/*
** EPITECH PROJECT, 2018
** PSU_ftrace_2017
** File description:
** my_list_count
*/

#include <stdlib.h>
#include "list.h"

size_t list_count(const list_t *list)
{
	size_t it = 0;

	while (list != NULL) {
		list = list->next;
		++it;
	}
	return (it);
}