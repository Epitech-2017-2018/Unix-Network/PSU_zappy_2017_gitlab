/*
** EPITECH PROJECT, 2018
** list
** File description:
** list
*/

#include "list.h"

void *list_get_by_elm(list_t *list, const void *elm)
{
	while (list) {
		if (list->elm == elm)
			return (list);
		list = list->next;
	}
	return (NULL);
}