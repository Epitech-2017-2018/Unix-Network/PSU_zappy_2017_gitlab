/*
** EPITECH PROJECT, 2018
** PSU_ftrace_2017
** File description:
** list_print
*/

#include <stdlib.h>
#include <stdbool.h>
#include "list.h"

void list_print(list_t *list, void (fnc_print)(void *))
{
	while (list != NULL) {
		fnc_print(list->elm);
		list = list->next;
	}
}