/*
** EPITECH PROJECT, 2018
** PSU_ftrace_2017
** File description:
** my_list_push
*/

#include <stdlib.h>
#include <stdbool.h>
#include "list.h"

bool list_push(list_t **list, void *element)
{
	list_t *node = malloc(sizeof(list_t));

	if (node == NULL)
		return (false);
	node->elm = element;
	node->next = NULL;
	if (*list == NULL) {
		node->prev = NULL;
		*list = node;
	}
	else {
		node->prev = (*list)->end;
		node->prev->next = node;
	}
	(*list)->end = node;
	return (true);
}
