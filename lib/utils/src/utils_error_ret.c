/*
** EPITECH PROJECT, 2018
** myftp
** File description:
** myftp
*/

#include "utils.h"

bool utils_error_ret(const char *s)
{
	fprintf(stderr, "%s\n", s);
	return (false);
}