/*
** EPITECH PROJECT, 2018
** myftp
** File description:
** myftp
*/

#include "utils.h"

bool utils_perror_ret(const char *s)
{
	perror(s);
	return (false);
}