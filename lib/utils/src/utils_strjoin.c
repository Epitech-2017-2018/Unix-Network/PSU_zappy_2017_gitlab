/*
** EPITECH PROJECT, 2018
** utils
** File description:
** utils
*/

#include "utils.h"

char *utils_strjoin(char * const *wt, const char *separator)
{
	char *ret;
	size_t len = 0;
	size_t sep_len = (separator) ? strlen(separator) : 0;
	size_t wt_count = utils_wt_count(wt);

	for (size_t it = 0; wt[it]; ++it)
		len += strlen(wt[it]);
	ret = malloc(sizeof(char) * ((len + sep_len * wt_count) + 1));
	if (!ret)
		return (NULL);
	ret[0] = 0;
	for (size_t it = 0; wt[it]; ++it) {
		if (it > 0 && separator)
			strcat(ret, separator);
		strcat(ret, wt[it]);
	}
	return (ret);
}