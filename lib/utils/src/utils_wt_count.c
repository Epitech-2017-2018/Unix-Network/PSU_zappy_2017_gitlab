/*
** EPITECH PROJECT, 2018
** irc
** File description:
** irc
*/

#include "utils.h"

size_t utils_wt_count(char * const *wt)
{
	size_t count = 0;

	if (!wt)
		return (0);
	for (; wt[count]; ++count);
	return (count);
}