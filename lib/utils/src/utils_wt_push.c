/*
** EPITECH PROJECT, 2018
** utils
** File description:
** utils
*/

#include "utils.h"

char **utils_wt_push(char **wt, const char *s)
{
	size_t len = utils_wt_count(wt);
	char **new;

	new = realloc(wt, sizeof(char *) * (len + 2));
	if (!new)
		return (NULL);
	new[len] = strdup(s);
	if (!new[len])
		return (NULL);
	new[len + 1] = NULL;
	return (new);
}