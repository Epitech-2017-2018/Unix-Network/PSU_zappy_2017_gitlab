/*
** EPITECH PROJECT, 2018
** Zappy
** File description:
** Unit Test retrieve_args.c
*/

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "ai.h"
#include "error.h"

Test(test_client_connect, test_client_connect_no_info, .init=cr_redirect_stderr)
{
	cr_assert_eq(connect_to_serv(NULL), -1);
	cr_assert_stderr_eq_str("zappy: Unable to connect. No information supplied.\n");
}

Test(test_client_connect, test_client_connect_no_co_info, .init=cr_redirect_stderr)
{
	ai_info_t info = {NULL, NULL};

	cr_assert_eq(connect_to_serv(&info), -1);
	cr_assert_stderr_eq_str("zappy: Unable to connect. No information supplied.\n");
}

Test(test_client_connect, test_client_connect_unknown_host, .init=cr_redirect_stderr)
{
	ai_info_t info = {NULL, NULL};
	const char str[] = "plop";

	ai_info_content_init(&info);
	info.team = NULL;
	info.connection->port = 0;
	info.connection->hostname = str;
	cr_assert_eq(connect_to_serv(&info), -1);
	cr_assert_stderr_eq_str("zappy: getaddrinfo: Name or service not known\n");
	ai_info_content_destroy(&info);
}

Test(test_client_connect, test_client_connect_unable_to_connect, .init=cr_redirect_stderr)
{
	ai_info_t info = {NULL, NULL};
	const char str[] = "localhost";

	ai_info_content_init(&info);
	info.team = NULL;
	info.connection->port = 0;
	info.connection->hostname = str;
	cr_assert_eq(connect_to_serv(&info), -1);
	cr_assert_stderr_eq_str("zappy: Unable to connect to localhost:0\n");
	ai_info_content_destroy(&info);
}

// Tests en cours de travaux. Boucle infini non debug. Ne pas decommenter pour le moment.
Test(test_client_connect, test_client)
{
	int pid = 0;
	int wstatus = 0;
	ai_info_t info = {NULL, NULL};
	const char str[] = "localhost";
	char *argv[2] = {strdup("test_cl_to_serv_connection"), NULL};

	ai_info_content_init(&info);
	info.team = NULL;
	info.connection->port = 4242;
	info.connection->hostname = str;
	pid = fork();
	if (pid == 0) {
		execve("./test_binaries/test_cl_to_serv_connection", argv, NULL);
		exit(EXIT_FAILURE);
	}
	sleep(1);
	connect_to_serv(&info);
	waitpid(pid, &wstatus, 0);
	cr_assert(WIFEXITED(wstatus));
	free(argv[0]);
	cr_assert_eq(WEXITSTATUS(wstatus), 0);
	ai_info_content_destroy(&info);
}