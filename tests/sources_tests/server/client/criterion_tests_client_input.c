/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** zappy
*/

#include <criterion/criterion.h>
#include "network.h"
#include "client.h"

Test(test_client_input, add_block)
{
	client_t client;

	memset(&client, 0, sizeof(client_t));
	for (int it = 0; it < 10; ++it)
		cr_assert_eq(client_input_add_block(&client, "Hello"), true);
	cr_assert_eq(client_input_add_block(&client, "Hello"), false);
}

Test(test_client_input, get_block)
{
	client_t client;
	char *block;

	memset(&client, 0, sizeof(client_t));
	cr_assert_eq(client_input_add_block(&client, "Hello"), true);
	cr_assert_eq(client_input_add_block(&client, "World"), true);
	block = client_input_get_block(&client, 0);
	cr_assert_eq(strcmp(block, "Hello"), 0);
	*block = 0;
	cr_assert_eq(strcmp(client_input_get_block(&client, 1), "World"), 0);
	cr_assert_eq(client_input_get_block(&client, 10), NULL);
	cr_assert_eq(client_input_get_block(&client, -4), NULL);
}

Test(test_client_input, get_free_block)
{
	client_t client;
	char *block;

	memset(&client, 0, sizeof(client_t));
	cr_assert_eq(client_input_get_free_block(&client), 0);
	cr_assert_eq(client_input_add_block(&client, "Hello"), true);
	cr_assert_eq(client_input_get_free_block(&client), 1);
	block = client_input_get_block(&client, 0);
	cr_assert_neq(block, NULL);
	*block = 0;
	cr_assert_eq(client_input_get_free_block(&client), 0);
	for (int it = 0; it < 10; ++it)
		cr_assert_eq(client_input_add_block(&client, "Hello"), true);
	cr_assert_eq(client_input_get_free_block(&client), -1);
}