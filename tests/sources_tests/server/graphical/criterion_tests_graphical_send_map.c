/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Graphical protocol unit tests.
*/

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "graphical.h"
#include "map.h"
#include "network.h"
#include "args.h"

Test(test_graphical_send_map, test_client_not_connected)
{
	bool res = false;
	server_t server;
	map_t map;
	client_t client;

	map.coords.x = 10;
	map.coords.y = 10;
	map.tiles = NULL;
	server.map = map;
	client.connected = false;
	res = graphical_send_map(&server, &client);
	cr_assert_eq(res, false);
}

Test(test_graphical_send_map, test_empty_map)
{
	bool res = false;
	server_t server;
	map_t map;
	client_t client;
	host_t host;

	host.fd = 1;
	map.coords.x = 0;
	map.coords.y = 0;
	map.tiles = NULL;
	server.map = map;
	client.connected = true;
	client.data = host;
	res = graphical_send_map(&server, &client);
	cr_assert_eq(res, true);
}

Test(test_graphical_send_map, test_map)
{
	bool res = false;
	server_t server;
	map_t map;
	client_t client;
	host_t host;
	args_t args;
	team_t team;
	player_t player;

	args.width = 5;
	args.height = 5;
	args.clients_nb = 0;
	args.teams = NULL;
	host.fd = 1;
	map_init(&args, &map);
	server.map = map;
	client.connected = true;
	client.data = host;
	team_init(&team, "lel");
	player_init(&player, &map, &team);
	++map.tiles->content.resources[RESOURCE_LINEMATE];
	cr_assert_neq(list_push(&map.tiles->content.players, &player), false);
	res = graphical_send_map(&server, &client);
	cr_assert_eq(res, true);
}

Test(test_graphical_send_map, test_map_with_player)
{
	bool res = false;
	server_t server;
	map_t map;
	client_t client;
	host_t host;
	args_t args;
	player_t player;
	list_t *players = NULL;
	team_t team;

	team.number = 1;
	args.width = 5;
	args.height = 5;
	args.clients_nb = 0;
	args.teams = NULL;
	host.fd = 1;
	map_init(&args, &map);
	server.map = map;
	memset(&player, 0, sizeof(player_t));
	player.tile = map.tiles;
	player.team = &team;
	list_push(&players, &player);
	server.map.tiles->content.players = players;
	client.connected = true;
	client.data = host;
	res = graphical_send_map(&server, &client);
	cr_assert_eq(res, true);
}

Test(test_graphical_send_map, test_dprintf_fail)
{
	bool res = false;
	server_t server;
	map_t map;
	client_t client;
	host_t host;
	args_t args;
	player_t player;
	list_t *players = NULL;
	team_t team;

	team.number = 1;
	args.width = 5;
	args.height = 5;
	args.clients_nb = 0;
	args.teams = NULL;
	host.fd = -1;
	map_init(&args, &map);
	server.map = map;
	memset(&player, 0, sizeof(player_t));
	player.tile = map.tiles;
	player.team = &team;
	list_push(&players, &player);
	server.map.tiles->content.players = players;
	client.connected = true;
	client.data = host;
	res = graphical_send_map(&server, &client);
	cr_assert_eq(res, false);
}