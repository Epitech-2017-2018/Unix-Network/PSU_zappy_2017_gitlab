/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** zappy
*/

#include <criterion/criterion.h>
#include "network.h"
#include "player.h"

Test(test_player, player_common)
{
	player_t player;
	map_t map;
	team_t team;
	args_t args;

	memset(&player, 0, sizeof(player_t));
	memset(&map, 0, sizeof(map_t));
	memset(&team, 0, sizeof(team_t));
	memset(&args, 0, sizeof(args_t));
	args.width = 20;
	args.height = 20;
	cr_assert_neq(map_init(&args, &map), false);
	team_init(&team, "Hello");
	cr_assert_eq(player_init(&player, &map, &team), true);
	player.tile = map_get_tile(&map, &(map_coordinate_t) { 10, 10 });
	cr_assert_eq(
		player_tile_number(&player, &(map_coordinate_t) { 11, 9 }), 8);
	player_destroy(&player);
	map_destroy(&map);
	team_destroy(&team);
}
