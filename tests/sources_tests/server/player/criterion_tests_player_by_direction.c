/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** zappy
*/

#include <criterion/criterion.h>
#include "network.h"
#include "player.h"

Test(test_player, player_tile_by_direction)
{
	player_t player;
	map_t map;
	team_t team;
	args_t args;

	memset(&player, 0, sizeof(player_t));
	memset(&map, 0, sizeof(map_t));
	memset(&team, 0, sizeof(team_t));
	memset(&args, 0, sizeof(args_t));
	args.width = 5;
	args.height = 5;
	cr_assert_neq(map_init(&args, &map), false);
	team_init(&team, "team");
	cr_assert_eq(player_init(&player, &map, &team), true);
	cr_assert_eq(player_tile_by_direction(&player, &(direction_t) { DIRECTION_NORTH }), player.tile->north);
	cr_assert_eq(player_tile_by_direction(&player, &(direction_t) { DIRECTION_WEST }), player.tile->west);
	cr_assert_eq(player_tile_by_direction(&player, &(direction_t) { DIRECTION_EAST }), player.tile->east);
	cr_assert_eq(player_tile_by_direction(&player, &(direction_t) { DIRECTION_SOUTH }), player.tile->south);
	player_destroy(&player);
	map_destroy(&map);
	team_destroy(&team);
}
