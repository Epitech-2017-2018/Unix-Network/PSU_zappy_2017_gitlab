/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** zappy
*/

#include <criterion/criterion.h>
#include "network.h"
#include "server.h"

Test(test_team, team_common)
{
	team_t team;
	const char *name1 = "team123";
	const char *name2 = ",ad";
	const char *name3 = "team?";

	cr_assert_eq(team_init(&team, name1), true);
	cr_assert_eq(strcmp(team.name, "team123"), 0);
	cr_assert_eq(team.number, 1);
	team_destroy(&team);
	cr_assert_eq(team_init(&team, name2), false);
	cr_assert_eq(team_init(&team, name3), false);
}
