/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** zappy
*/

#include <criterion/criterion.h>
#include "network.h"
#include "server.h"

Test(test_team, team_utils)
{
	list_t *teams = NULL;
	team_t team;
	team_t other;

	team_init(&team, "team");
	cr_assert_eq(list_push(&team.members, &(client_t) { 0 }), true);
	cr_assert_eq(team_count_members(&team), 1);
	cr_assert_eq(team_add(&teams, &team), true);
	cr_assert_eq(team_add(&teams, &team), false);
	cr_assert_neq(team_by_name(teams, "team"), NULL);
	cr_assert_eq(team_by_name(teams, "superlel"), NULL);
	cr_assert_eq(team_exists(&teams, &team), true);
	other.name = strdup("lel");
	other.number = 1;
	cr_assert_eq(team_exists(&teams, &other), true);
	other.number = 0;
	cr_assert_eq(team_exists(&teams, &other), false);
	list_destroy(&teams, LIST_FREE_NOT, NULL);
	team_destroy(&team);
	free(other.name);
}
