#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

int main()
{
	int fd = 0;
	struct sockaddr_in addr;
	struct sockaddr_in c_addr;
	int fd2 = 0;
	socklen_t len = 0;

	fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(4242);
	inet_aton("127.0.0.1", &(addr.sin_addr));
	bind(fd, (struct sockaddr *)&addr, sizeof(addr));
	listen(fd, 1);
	c_addr.sin_family = AF_INET;
	c_addr.sin_port = htons(4242);
	inet_aton("127.0.0.1", &(c_addr.sin_addr));
	fd2 = accept(fd, (struct sockaddr *)&c_addr, &len);
	setsockopt(fd2, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int));
	close(fd);
	close(fd2);
	return (0);
}
