/*
** EPITECH PROJECT, 2018
** Zappy
** File description:
** Define Ai's Header
*/
/**
* \file
* \brief Define AI's Header
*/

#ifndef AI_H_
# define AI_H_

# include "error.h"
# include "connection.h"

/**
* \name s_ai_info
* \brief information about the ai
*/
struct ai_info
{
	const char *team; /*!< TEAM name of the team
(reference to the given argument. DO NOT FREE !!)*/
	connection_t *connection; /*!< CONNECTION structure containing
the information about the connection to the server.*/
};

typedef struct ai_info ai_info_t;
int ai_info_content_init(ai_info_t *info);
int ai_info_content_destroy(ai_info_t *info);
int connect_to_serv(ai_info_t *info);

typedef void (*fetch_data)(ai_info_t *info, const char *argument);

# define NB_INFO 3

void fetch_port(ai_info_t *info, const char *port);
void fetch_team(ai_info_t *info, const char *team);
void fetch_hostname(ai_info_t *info, const char *hostname);

static const fetch_data fetch_client_info[NB_INFO] = {
	fetch_port,
	fetch_team,
	fetch_hostname
};

int display_ai_help_message(const char *binary);
int retrieve_args(int ac, const char **av, ai_info_t *info);

/* ai help messages */
static const char AI_USAGE[] = "USAGE: %s -p po\
rt -n name -h machine\n\n";
static const char AI_PORT[] = "\tport\t\tis the port number\n";
static const char AI_TEAM[] = "\tname\t\tis the name of the team\n";
static const char AI_HOSTNAME[] = "\tmachine\t\tis the name of the ma\
chine; localhost by default\n";

char **ai_cmdlist(char **ai_cmdlist_h);
char *ai_cmdlist_pop_element(void);
int ai_cmdlist_add_element(char const *string);
int send_to_serv(char const *str);

static const int WR_MASK = 0x0FFF0000;

# define MAX_RES_SIZE	4096

int saved_fd(int sfd);
int is_available(void);
int main_connection(int argc, char const **argv);
int get_info_from_serv(int sfd);
int clean_ressources(void);

#endif /* !AI_H_ */