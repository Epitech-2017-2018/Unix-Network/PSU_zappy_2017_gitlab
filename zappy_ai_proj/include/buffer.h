/*
** EPITECH PROJECT, 2018
** PSU_zappy_2017
** File description:
** Circular buffer header file
*/
/**
* \file
* \brief Circular buffer header file.
*/

#ifndef BUFFER_H_
# define BUFFER_H_

# define RECV_SIZE 8164
# define READ_SIZE RECV_SIZE/2

/**
* \name buffer_s
* \brief Ring buffer informations.
*/
typedef struct buffer_s
{
	char buf[RECV_SIZE]; /*!< buf The ring buffer.*/
	int r; /*!< r The position of the reading head.*/
	int w; /*!< w The position of the writing head.*/
	int size; /*!< size The size of the buffer.*/
} buffer_t;

buffer_t *ai_buffer_get(void);
int ai_buffer_add_input(buffer_t *buf, char *tmp, int r);

#endif /* ! BUFFER_H_ */
