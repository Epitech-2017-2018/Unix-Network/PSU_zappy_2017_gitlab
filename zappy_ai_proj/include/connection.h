/*
** EPITECH PROJECT, 2018
** PSU_zappy_2017
** File description:
** Connection header
*/

/**
* \file
* \brief Connection header file
*/

#ifndef CONNECTION_H_
# define CONNECTION_H_

static const int OK_READ = 1;
static const int OK_WRITE = 2;
/**
* \name s_connection
* \brief connection information of the server
*/
struct s_connection
{
	int port;/*!< PORT connection port of the server*/
	const char *hostname;/*!< HOSTNAME hostname of the server
(reference to the given argument. DO NOT FREE !!)*/
	int sfd;/*!< SFD socket for direct
connection to the server*/
};

typedef struct s_connection connection_t;

#endif /* !CONNECTION_H_ */
