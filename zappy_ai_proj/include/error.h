/*
** EPITECH PROJECT, 2018
** Zappy
** File description:
** Define Error's Header
*/
/**
* \file
* \brief Define Error's Header
*/

#ifndef ERROR_H_
# define ERROR_H_

static const int OK = 0;
static const int KO = 84;
static const int ERR_RET = -1;

/* Messages send when an error rise on getting argument. */
static const char CL_INV_ARG_NB[] = "zappy: Invalid number of argument.";
static const char CL_INV_ARG_ORD[] = "zappy: Invalid argument order.";
static const char CL_INV_ARG_UNFOLLOWED[] = "zappy: option %s is not foll\
owed by an argument.\n";
static const char CL_INV_ARG_OPT_FOLLOW[] = "zappy: option %s must be foll\
owed by an argument.\n";
static const char CL_INV_ARG_OPT_P_NUM[] = "zappy: option -p must be foll\
owed by an argument composed only by digits.";
static const char CL_INV_ARG_PORT_VAL[] = "zappy: port value must be over th\
e reserved interval ( >= 1024).";
static const char CL_INV_ARG_OPT_N[] = "zappy: option -n can't start by a\
n at symbol (\"@\").";
static const char CL_INV_ARG_OPT[] = "zappy: option -%c is not a valid one.\n";
static const char CL_INV_CO_NO_INFO[] = "zappy: Unable to connect. No i\
nformation supplied.";
static const char CL_INV_CO_NO_CO_INFO[] = "zappy: Unable to connect. No con\
nection information supplied.";

static const char ERR_CL_GETADDR[] = "zappy: getaddrinfo: %s\n";
static const char ERR_CL_CONTENT_DESTROY[] = "zappy: a\
i_info_destroy: Invalid ptr.";
static const char ERR_CL_CONTENT_INIT[] = "zappy: ai_info_init: Invalid ptr.";
static const char ERR_CL_INIT_ALLOC[] = "zappy: ai_info_init: malloc:";
static const char ERR_CL_CONNECTION[] = "zappy: Unable to connect to %s:%d\n";
static const char ERR_CL_CMD_NOTFULLYSEND[] = "zappy: A comma\
nd was not fully send.";

#endif /* !ERROR_H_ */