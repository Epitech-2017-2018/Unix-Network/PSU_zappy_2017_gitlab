#!/usr/bin/python3.6

from sys import path as env_path
from os import path as os_path
from os import urandom
from NetworkCommunication import NetworkCommunication
from Tools import Tools
from Broadcast import BroadcastAi
from re import compile, match
from time import time

PATTERN_BROADCAST = compile("message\s[a-zA-Z0-9]+,\s([a-zA-Z0-9\s]+)")
PATTERN_ELEVATION_LEVEL = compile("Current level: [2-8]")
PATTERN_MAP = compile("(\[|\[\s+)(([a-zA-Z0-9-\s]+)(,\s+|,+|))*(\]|\s+\])")
PATTERN_EJECT = compile("eject: [0-9]")
PATTERN_MESSAGE = compile("message [0-8]")

UNITS_TIME_FOOD = 126

NF_DIRECTION = [["Left"], ["Left", "Left"], ["Right"]]

CONVERT = ["linemate", "deraumere", "sibur", "mendiane", "phiras",
	"thystame", "food"]

ACTIONS = ["Left", "Right", "Forward", "Take food"]

FOOD_NEED = {1: 800, 2: 772, 3: 744, 4: 716, 5: 688, 6: 660, 7: 632}

STONE_NEEDS = {1: [1, 0, 0, 0, 0, 0], 2: [1, 1, 1, 0, 0, 0],
	3: [2, 0, 1, 0, 2, 0], 4: [1, 1, 2, 0, 1, 0],
	5: [1, 2, 1, 3, 0, 0], 6: [1, 2, 3, 0, 1, 0], 7: [2, 2, 2, 2, 2, 1]}

PLAYERS_NEEDS = {1: 1, 2: 2, 3: 2, 4: 4, 5: 4, 6: 6, 7: 6}

REACH_ELEVATION_MOVES = {1: ["Forward"],
			2: ["Forward", "Left", "Forward"],
			3: ["Left", "Forward"],
			4: ["Left", "Forward", "Left", "Forward"],
			5: ["Left", "Left", "Forward"],
			6: ["Right", "Forward", "Right", "Forward"],
			7: ["Right", "Forward"],
			8: ["Forward", "Right", "Forward"]}

def find_path(to_find):
	"""Find path of the C shared library

	Args:
		to_find: The library name to find, with a '/' at the beginning

	Raise:
		Raise an exception if the file haven't been found.

	Return:
		Return the direct path to the library if found
		and an empty string if not
	"""

	if os_path.isfile("." + to_find):
		return ("." + to_find)
	for paths in env_path.copy():
		if os_path.isfile(paths + to_find):
			return (paths + to_find)
	raise FileNotFoundError("Err")

class Trantorians:
	"""Trantorians class which is an AI.

	Attributes:
		_network: network class used to communicate.
		_type: enum class which descries the trantorians type.
		_dict: dictionnary which contains the functions to call on
		the return of a command.
		_dictNetwork: dictionnary which contains the network functions
		used to call C types functions.
		_toSearch: element to be searched by the trantorians.
		_level: level of the trantorians.
		_clientNum: number of trantorians we can still connect
		(can change during the execution without changing the var).
		_teamName: name of the trantorian's team
		_responseQueue: Queue of last requests made by the trantorians
	"""

	def __init__(self):
		"""Init the class trantorians"""
		self._dict = {"Welcome": self._identification,
		"dead": self._dead,
		"Forward": self._action,
		"Right": self._action,
		"Left": self._action,
		"Take food": self._action, "Take linemate": self._action,
		"Take deraumere": self._action, "Take sibur": self._action,
		"Take mendiane": self._action, "Take phiras": self._action,
		"Take thystame": self._action,
		"Set food": self._action, "Set linemate": self._action,
		"Set deraumere": self._action, "Set sibur": self._action,
		"Set mendiane": self._action, "Set phiras": self._action,
		"Set thystame": self._action,
		"Connect_nbr": self._connectNbr,
		"Fork": self._action,
		"Eject": self._action,
		"EjectReceived": self._eject,
		"Inventory": self._inventory,
		"Look": self._look,
		"BroadcastSend": self._action,
		"BroadcastReceived": self._broadcast,
		"Incantation": self._incantation}
		self._teamName = ""
		self._timeConnect = 0
		self._clientNum = 0
		self._broadcastString = []
		self._awaitedPlayer = 0
		self._toSearch = ["food"]
		self._level = 1
		self._unexpected = False
		self._foodNb = 0
		self._nbPlayersSameLevel = 0
		self._isWaiting = False
		self._timeCmdTeamLevel = 0
		self._onlyFood = False
		self._inIncantation = False
		self._playerForIncantation = []
		self._isManager = False
		self._incantationLoop = 0
		self._responseQueue = []
		self._sendArrived = False
		self._firstBroadcastMove = True

	def _command(self, cmd, **kwargs):
		"""Call to execute a function of the shared library.

		Args:
			cmd: command to execute.
			nbArgs: number of arguments in the next parameter.
			**kwargs: tab of args
		"""

		try:
			return self._dictNetwork[cmd](**kwargs)
		except Exception as e:
			self._deadWithError(e)

	def _identification(self, answer):
		"""Identify the client after received "WELCOME".

		Args:
			answer: the string received as answer of the command.

		Exit:
			Exit with 84 value if an error occured at the the
			connection.

		Return:
			Return True if no errors occured, false otherwise.
		"""

		try:
			self._command("send", arg1=self._teamName)
			answer = self._command("get")
			self._clientNum = int(answer)
			self._timeConnect = time()
			answer = self._command("get")
		except:
			self._deadWithError("Couldn't connect to the server.")
		self._responseQueue.append("Inventory")
		return True

	def connectMe(self, argc, argv):
		"""Connect the trantorian to the server.

		Args:
			self: the concerned class.
			argc: number of arguments.
			argv: tab of arguments.

		Exit:
			Exit 84 if an errors occured at the creation of
			the network module.

		Return:
			Return True if no errors occured, False otherwise
		"""

		try:
			path = find_path("/libmyc.so")
			self._network = NetworkCommunication(path)
		except:
			self._deadWithError("Can't create the shared library.")
		self._dictNetwork = {"send": self._network.sendCommand,
		"connect": self._network.connectToServer,
		"available": self._network.isAvailable,
		"get": self._network.getCommand,
		"clean": self._network.cleanRessources}
		self._command("connect", arg1=argc, arg2=argv)
		self._teamName = argv[argv.index("-n") + 1]
		answer = self._command("get")
		self._responseQueue = list(["Welcome"])
		self._dict[self._responseQueue.pop(0)](answer)

	def _getCommandAndCheckIt(self):
		"""Function to Get a command send by the server and check it.

		Return:
			Return the getting string.
		"""

		answer = self._command("get")
		try:
			self._checkEntry(answer)
		except SyntaxError as SE:
			self._deadWithError(SE)
		return answer

	def _executeFirstCommandInQueue(self, answer):
		"""Execute a function corresponding to the string find in
		self._responsequeue

		Args:
			answer: String coming from the server.
		"""

		try:
			cmd = self._responseQueue.pop(0)
			if self._dict[cmd](answer) == False:
				return self._deadWithError("An error occured.")
		except SyntaxError as SE:
			self._deadWithError(SE)
		except:
			self._deadWithError("No further operations...")

	def live(self):
		"""Functions which start the AI.

		The AI needs to be connected first.

		Returns:
			Return False if a program error occured in the life
			of the trantorians. True otherwise.
		"""

		while 1:
			if len(self._responseQueue) == 0:
				self._responseQueue = ["Inventory"]
			cmd = self._responseQueue[0]
			if cmd == "BroadcastSend":
				cmd = "Broadcast " + self._broadcastString[0]
				self._broadcastString.pop(0)
			self._command("send", arg1=cmd)
			answer = self._getCommandAndCheckIt()
			while self._unexpected == True:
				self._executeFirstCommandInQueue(answer)
				answer = self._getCommandAndCheckIt()
			if self._sendArrived == False:
				self._executeFirstCommandInQueue(answer)
			self._sendArrived = False
		return True

	def _connectNbr(self, answer):
		"""Call when we received the number of remainings connections.

		Args:
			answer: String received.

		Return:
			Return true if no errors occured, False otherwise.
		"""

		try:
			nb = int(answer)
		except:
			self._deadWithError("Bad Connect_nbr answer.")
		if nb == 0:
			self._responseQueue.append("Fork")
		return True

	def _prepareElevation(self):
		"""Prepare elevation

		Return:
			tab: Tab containing what we need to search
		"""

		if self._nbPlayersSameLevel < PLAYERS_NEEDS[self._level] - 1:
			self._responseQueue.append("Connect_nbr")
			self._responseQueue.append("BroadcastSend")
			string = BroadcastAi.requestTeamPlayerSameLevel(self)
			self._broadcastString.append(string)
			self._foodNb = 3
			if "Inventory" not in self._responseQueue:
				self._responseQueue.append("Inventory")
			return ["food"]
		else:
			self._isManager = True
			if self._awaitedPlayer >= \
			PLAYERS_NEEDS[self._level] - 1:
				if "Inventory" not in self._responseQueue:
					self._responseQueue.append("Inventory")
				return ["elevation"]
			self._responseQueue.append("BroadcastSend")
			string = BroadcastAi.requestMove(self)
			self._broadcastString.append(string)
			if "Inventory" not in self._responseQueue:
				self._responseQueue.append("Inventory")
			return ["food"]

	def _findStone(self, inventory):
		"""Fill a tab containing the stones we need to search or
		prepare	elevation if needed

		Return:
			tab: Tab containing the stones we need to search
		"""

		inventory.pop(0)
		level_needs = STONE_NEEDS[self._level]
		tab = []
		for i in range(0, 6):
			if inventory[i] < level_needs[i]:
				tab.append(CONVERT[i])
		if len(tab) == 0:
			if self._level > 1:
				return self._prepareElevation()
			self._responseQueue.append("Look")
			return ["elevation"]
		self._responseQueue.append("Look")
		return tab

	def _checkEntry(self, line):
		"""Check the entry command received.

		Args:
			line: the line received.

		Raise:
			Raise a SyntaxError if the command is invalid.
		"""

		self._unexpected = False
		if line == "ok" or line == "ko":
			return
		if line == "Elevation underway":
			self._responseQueue = ["Incantation", "Incantation"]
			self._unexpected = True
			return
		strErr = "The received command is invalid: " + line
		tmp = match(PATTERN_MAP, line)
		if tmp != None:
			return
		tmp = match(PATTERN_EJECT, line)
		if tmp != None:
			self._unexpected = True
			self._responseQueue.insert(0, "EjectReceived")
			return
		tmp = match(PATTERN_BROADCAST, line)
		if tmp != None:
			self._unexpected = True
			self._responseQueue.insert(0, "BroadcastReceived")
			return
		try:
			tmp = match(PATTERN_ELEVATION_LEVEL, line)
			if tmp != None:
				return
		except:
			raise SyntaxError(strErr)
		if line == "dead":
			self._unexpected = True
			self._responseQueue.insert(0, "dead")
			return
		try:
			int(line)
			return
		except:
			raise SyntaxError(strErr)
		raise SyntaxError(strErr)

	def _searchItem(self, itemToSearch, inventory):
		"""Ask to the server to look for the given itemToSearch.

		Args:
			itemToSearch: items to look for.
		"""

		if itemToSearch == "food":
			self._toSearch = ["food"]
			self._responseQueue.append("Look")
		elif itemToSearch == "stones":
			self._toSearch = self._findStone(inventory)

	def _inventory(self, answer):
		"""Call when the inventory is return.

		Args:
			answer: string describing the inventory.

		Exit:
			Exit with 84 if the given line isn't good.

		Return:
			Return True if no errors occured, False otherwise.
		"""

		if answer == "ok" or answer == "ko":
			self._responseQueue.append("Inventory")
			return
		answer = Tools._cleanInventory(answer)
		if answer[0] * UNITS_TIME_FOOD < FOOD_NEED[self._level] \
		or self._onlyFood == True:
			if self._isManager:
				self._managerReset("")
			self._searchItem("food", answer)
			if self._foodNb > 0:
				self._foodNb -= 1
		elif self._isWaiting:
			if "Inventory" not in self._responseQueue:
				self._responseQueue.append("Inventory")
		elif self._foodNb > 0:
			self._searchItem("food", answer)
			if self._foodNb > 0:
				self._foodNb -= 1
		elif len(self._toSearch) > 0 \
		and self._toSearch[0] == "elevation":
			self._responseQueue.append("Look")
		elif self._isManager:
			self._searchItem("stones", answer)
		else:
			if "food" in self._toSearch:
				self._toSearch.remove("food")
			self._searchItem("stones", answer)
		return True

	def _findAnotherWay(self):
		"""Find another way to find the item searched"""

		direction = int(urandom(1)[0]) % 3
		moves = NF_DIRECTION[direction]
		for move in moves:
			self._responseQueue.append(move)
		self._responseQueue.append("Forward")
		if "Inventory" not in self._responseQueue:
			self._responseQueue.append("Inventory")

	def _accessItem(self, rank, dist, item, count):
		"""Function to access the needed item

		Args:
			rank: Item's line where we founded it on look
			dist: Distance from middle where the item resides
			item: Searched item we founded
			count: Number of item we found
		"""

		for i in range(0, rank):
			self._responseQueue.append("Forward")
		if dist != 0:
			side = "Left" if dist < 0 else "Right"
			if dist < 0:
				dist *= -1
			self._responseQueue.append(side)
			for i in range(0, dist):
				self._responseQueue.append("Forward")
		for i in range(0, count):
			cmd = "Take " + item
			self._responseQueue.append(cmd)

	def _findItem(self, answer, item):
		"""Function to find the searched item in front of us

		Args:
			answer: String describing the look command.
			item: Searched item we founded
		"""

		answer = answer.replace(", ", ",")
		tab_look = answer.split(",")
		tile = 0
		for line in tab_look:
			count = line.count(item)
			if count > 0:
				break
			tile += 1
		rank, dist = Tools._convertTileToSeq(tile)
		self._accessItem(rank, dist, item, count)

	def _handleElevation(self, answer):
		"""Function to clean the ground and try an elevation

		Args:
			answer: String describing the look command.
		"""

		tile0 = (answer.split(",")[0]).split(" ")
		if tile0.count("player") != PLAYERS_NEEDS[self._level]:
			self._managerReset("Eject")
			return
		answer = answer.replace(", ", ",")
		tile = answer.split(",")[0]
		for elem in tile.split(" "):
			if elem != "player" and elem != "food":
				cmd = "Take " + elem
				self._responseQueue.append(cmd)
		needs = STONE_NEEDS[self._level]
		for i in range(0, 6):
			for needed in range(0, needs[i]):
				cmd = "Set " + CONVERT[i]
				self._responseQueue.append(cmd)
		self._responseQueue.append("Incantation")

	def _look(self, answer):
		"""Function to treat the return of the look command.

		Args:
			answer: String describing the look command.

		Return:
			Return True if no errors occured, false otherwise.
		"""

		answer = Tools._cleanLook(answer)
		if len(self._toSearch) == 1 \
		and self._toSearch[0] == "elevation":
			self._handleElevation(answer)
			return True
		tile = -1
		for objective in self._toSearch:
			if answer.find(objective) > 0:
				tile = self._toSearch.index(objective)
				break
		if tile < 0:
			self._findAnotherWay()
			return True
		self._findItem(answer, self._toSearch[tile])
		return True

	def _action(self, answer):
		"""Call to know if an action has been done.

		Called after an action which return ok or ko has been called.

		Args:
			answer: string which is ok or ko

		Return:
			Return True because no errors could occurs.
		"""

		if len(self._responseQueue) == 0:
			self._responseQueue.append("Inventory")
		return True

	def _broadcast(self, answer):
		"""Call when we received a broadcast.

		Args:
			answer: Broadcast received.

		Return:
			Return True if no sys errors occured, False otherwise.
		"""

		line = answer.split(",")
		if self._inIncantation:
			return
		if len(line) < 3:
			return
		line[1] = line[1][1:]
		if match(PATTERN_MESSAGE, line[0]) == None \
		or line[1] != self._teamName:
			return
		if line[2].startswith("Level "):
			self._broadcastLevelAsk(line)
		elif line[2].startswith("Move "):
			self._broadcastMove(line)
		elif line[2].startswith("Iamlevel "):
			self._broadcastLevelAnswer(line)
		elif line[2].startswith("Here"):
			self._broadcastOnPosition(line)
		elif line[2].startswith("Reset "):
			self._broadcastReset(line)
		elif line[2].startswith("Accept"):
			self._broadcastAccept(line)
		return True

	def _broadcastAccept(self, line):
		"""Call when we received an acceptation broadcast

		Args:
			line: broadcast split to ","
		"""

		if len(line) != 6:
			return
		msg = line[3].split(" ")
		if len(msg) != 2:
			return
		try:
			if float(msg[0]) != self._timeConnect \
			or int(msg[1]) != self._clientNum:
				return
		except:
			return
		self._onlyFood = True
		if len(self._responseQueue) > 0:
			self._responseQueue = [self._responseQueue[0]]
			if self._responseQueue[0] != "BroadcastSend":
				self._broadcastString = []
		else:
			self._broadcastString = []
		self._responseQueue.append("Inventory")
		return

	def _broadcastReset(self, line):
		"""Call when we received a level Broadcast

		Args:
			Line: the broadcast split on ","
		"""

		if len(line) != 6:
			return
		players = line[4].split(":")
		isMe = False
		try:
			for player in players:
				aPlayer = player.split(" ")
				if float(aPlayer[0]) == self._timeConnect\
				and int(aPlayer[1]) == self._clientNum:
					isMe = True
		except:
			return
		if isMe == False:
			return
		self._resetTrantorians()

	def _broadcastLevelAsk(self, line):
		"""Call when we received a level Broadcast

		Args:
			Line: the broadcast split on ","
		"""

		if len(line) != 5:
			return
		msg = line[2].split(" ")
		if len(msg) != 2 or msg[1].isdigit() == False:
			return
		try:
			if int(msg[1]) != self._level:
				return
		except:
			return
		answer = BroadcastAi.answerTeamPlayerSameLevel(self, \
		line[3], line[4])
		self._responseQueue.append("BroadcastSend")
		self._broadcastString.append(answer)
		if "Inventory" not in self._responseQueue:
			self._responseQueue.append("Inventory")
		return

	def _broadcastLevelAnswer(self, line):
		"""Call when we received a broadcast level answer.

		Args:
			line: the broadcast split on ",".
		"""

		if len(line) != 6:
			return
		msg = line[2].split(" ")
		my_id = line[3].split(" ")
		other_id = line[4].split(" ")
		if len(msg) != 2 or msg[1].isdigit() == False \
		or int(msg[1]) != self._level:
			return
		if len(my_id) != 2 or len(other_id) != 2:
			return
		try:
			if float(my_id[0]) != self._timeConnect \
			or float(my_id[1]) != self._clientNum \
			or float(line[5]) != self._timeCmdTeamLevel:
				return
			if other_id not in self._playerForIncantation \
			and self._nbPlayersSameLevel \
			< PLAYERS_NEEDS[self._level] - 1:
				self._responseQueue.append("BroadcastSend")
				self._broadcastString.append(BroadcastAi\
				.answerAcceptYou(self, other_id))
				self._playerForIncantation.append(other_id)
				self._nbPlayersSameLevel += 1
		except:
			return
		return

	def _broadcastMove(self, line):
		"""Call when we received a level Broadcast

		Args:
			Line: the broadcast split on ","
		"""

		if len(line) != 6:
			return
		try:
			listPlayer = line[4].split(":")
			if int(line[2].split(" ")[1]) != self._level:
				return
		except:
			return
		isMyTeam = False
		try:
			for player in listPlayer:
				ids = player.split(" ")
			if len(ids) != 2:
				return
			if float(ids[0]) == self._timeConnect \
			and int(ids[1]) == self._clientNum:
				isMyTeam = True
		except:
			return
		if isMyTeam == False:
			return
		try:
			tile = int(line[0].split(" ")[1])
		except:
			return
		if self._firstBroadcastMove:
			self._firstBroadcastMove = False
			if len(self._responseQueue) > 0:
				tmp = self._responseQueue[0]
				self._responseQueue = [tmp]
			self._onlyFood = False
			self._isWaiting = True
		if tile == 0:
			string = BroadcastAi.answerOnPosition(self, \
			line[3], line[5])
			self._broadcastString.append(string)
			self._responseQueue = ["BroadcastSend", "Inventory"]
			self._sendArrived = True
			return
		if (len(self._responseQueue) > 0 \
		and self._responseQueue[0] in ACTIONS):
			return
		for moves in REACH_ELEVATION_MOVES[tile]:
			self._responseQueue.append(moves)
		return

	def _broadcastOnPosition(self, line):
		"""Call when we received a on position.

		Args:
			Line: the broadcast split on ","
		"""

		if len(line) != 6:
			return
		my_id = line[3].split(" ")
		other_id = line[4].split(" ")
		try:
			if float(my_id[0]) != self._timeConnect \
			or float(my_id[1]) != self._clientNum:
				return
			self._awaitedPlayer += 1
		except:
			return

	def _eject(self, answer):
		"""Call when we received eject from an other player

		Args:
			answer: String received.

		Return:
			Return True or false if an error occured.
		"""

		if self._isManager:
			self._managerReset("")
		return True

	def _incantation(self, answer):
		"""Call incantation when we are incanting.

		Args:
			answer: received command.

		Return:
			Return True if no errors occured, False otherwise.
		"""

		if answer == "dead":
			self._dead("dead")
		while answer != "ko" \
		and answer.startswith("Current level: ") == False \
		and answer != "Elevation underway":
			answer = self._command("get")
			if answer == "dead":
				self._dead("dead")
		self._incantationLoop += 1
		if answer == "Elevation underway":
			self._inIncantation = True
		else:
			self._inIncantation = False
		try:
			tmp = match(PATTERN_ELEVATION_LEVEL, answer)
			if tmp != None:
				self._level = int(answer[-1:])
				self._toSearch = ["food"]
			if "Inventory" not in self._responseQueue:
				self._responseQueue.append("Inventory")
		except:
			raise SyntaxError("Invalid level")
		if answer == "ko" and self._incantationLoop == 1:
			if "Inventory" not in self._responseQueue:
				self._responseQueue.append("Inventory")
			cmd = "message "
			while cmd != "ko":
				cmd = self._command("get", arg1=True)
			if cmd == "ko":
				return self._incantation(cmd)
			self._deadWithError("Unknown command: {}\
(Incantation).".format(cmd))
		if answer == "ko" or answer.startswith("Current level: "):
			if self._isManager:
				self._managerReset("")
		if answer == "Elevation underway":
			return
		self._incantationLoop = 0
		return True

	def _dead(self, answer):
		"""Called to end the client when he is dead or the server
		is down.

		Args:
			answer: the string received as answer of the command.

		Exit:
			Exit the client properly
		"""

		try:
			self._command("clean")
		except:
			print("Game: %s" % answer)
			exit(0)
		print("Game: %s" % answer)
		exit(0)

	def _deadWithError(self, stringToReturn):
		"""Called to end the client when and error occured.

		Args:
			answer: the string to display as error.

		Exit:
			Exit the client properly with 84 value.
		"""

		print("Exit: %s" % stringToReturn)
		exit(84)

	def _resetTrantorians(self):
		"""Reset a trantorian (Manager or not)
		"""

		self._awaitedPlayer = 0
		self._isWaiting = False
		self._isManager = False
		self._nbPlayersSameLevel = 0
		self._inIncantation = False
		self._playerForIncantation = []
		self._onlyFood = False
		self._firstBroadcastMove = True
		if len(self._responseQueue) > 0:
			tmp = self._responseQueue[0]
			self._responseQueue = [tmp]
			if self._responseQueue[0] != "BroadcastSend":
				self._broadcastString = []
		else:
			self._responsequeue = ["Inventory"]
		self._toSearch = ["food"]

	def _managerReset(self, cmd):
		"""Reset the manager and all of his teams"""

		self._awaitedPlayer = 0
		self._isWaiting = False
		self._isManager = False
		self._nbPlayersSameLevel = 0
		self._inIncantation = False
		self._onlyFood = False
		self._firstBroadcastMove = True
		if len(self._responseQueue) > 0:
			tmp = self._responseQueue[0]
			self._responseQueue = [tmp]
			self._responseQueue.append("BroadcastSend")
			self._broadcastString .append(BroadcastAi.\
			requestReset(self))
		else:
			self._responsequeue = ["BroadcastSend"]
			self._broadcastString = [BroadcastAi.\
			requestReset(self)]
		if cmd == "Eject":
			self._responseQueue.append("Eject")
		self._responseQueue.append("Inventory")
		self._playerForIncantation = []
		self._toSearch = ["food"]

	def getTeamName(self):
		"""Get the teamn name.

		Return:
			Return the team name.
		"""

		return self._teamName

	def getLevel(self):
		"""Get the Level.

		Return:
			Return the level.
		"""

		return self._level

	def getTimeConnect(self):
		"""Get the connection time

		Return:
			Return the time when the trantorians connected to
			the server.
		"""

		return self._timeConnect

	def getClientNum(self):
		"""Get the trantorians number.

		Return:
			Return the trantorians number
		"""

		return self._clientNum

	def getPlayerForIncantation(self):
		"""Get trantorians players list.

		Return:
			Return the list of concerned trantorians.
		"""

		return self._playerForIncantation

	def setTimeCmdTeamLevel(self, timeCmd):
		"""Set the time of the last request of team player of same
		level."""

		self._timeCmdTeamLevel = timeCmd
