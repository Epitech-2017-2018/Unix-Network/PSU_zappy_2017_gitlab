#!/usr/bin/python3.6

from time import time
from Broadcast import BroadcastAi
from Trantorians import Trantorians

def testRequestTeamPlayerSameLevel():
	timeTmp = time()
	tantor = Trantorians()
	tantor._teamName = "first"
	tantor._timeConnect = timeTmp
	tantor._clientNum = 2
	cmd = "first,Level 1," + str(timeTmp) + " 2,"
	request = BroadcastAi.requestTeamPlayerSameLevel(tantor)
	assert request.startswith(cmd)

def testAnswerOnPosition():
	timeTmp = time()
	tantor = Trantorians()
	tantor._teamName = "first"
	tantor._timeConnect = timeTmp
	tantor._clientNum = 2
	id = "12"
	cmd = "first,Here,12," + str(timeTmp) + " 2," + str(timeTmp)
	request = BroadcastAi.answerOnPosition(tantor, id, str(timeTmp))
	assert request == cmd

def testAnswerTeamPlayerSameLevel():
	timeTmp = time()
	tantor = Trantorians()
	tantor._teamName = "first"
	tantor._timeConnect = timeTmp
	tantor._clientNum = 2
	timeTmp = str(timeTmp)
	id = "12"
	cmd = "first,Iamlevel 1,12," + timeTmp + " 2," + timeTmp
	request = BroadcastAi.answerTeamPlayerSameLevel(tantor, id, timeTmp)
	assert request == cmd

def testRequestMove():
	timeTmp = time()
	tantor = Trantorians()
	tantor._teamName = "first"
	tantor._timeConnect = timeTmp
	tantor._clientNum = 2
	cmd = "first,Move 1," + str(timeTmp) + " 2,"
	request = BroadcastAi.requestMove(tantor)
	assert request.startswith(cmd)

def testAnserAcceptYou():
	tantor = Trantorians()
	tantor._teamName = "Team"
	tantor._timeConnect = time()
	tantor._clientNum = 1
	newTime = str(time())
	tantorID = [newTime, str(12)]
	ref = "Team,Accept," + str(newTime) + " 12," + str(tantor._timeConnect)
	msg = BroadcastAi.answerAcceptYou(tantor, tantorID)
	assert msg.startswith(ref)
