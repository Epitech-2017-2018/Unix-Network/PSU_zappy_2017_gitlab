#!/usr/bin/python3.6

from sys import argv
from sys import path
path.insert(0, "src/ai_algo/")
path.insert(0, "zappy_ai_proj/src/ai_algo/")
from Trantorians import Trantorians

if __name__ == "__main__":
	tantor = Trantorians()

	try:
		tantor.connectMe(len(argv), argv)
	except (ConnectionError, FileNotFoundError) as CE:
		exit(84)

	if tantor.live() == False:
		exit(84)
	exit(0)