/*
** EPITECH PROJECT, 2018
** PSU_myirc_2017
** File description:
** Functions relative to the buffer_t struct
*/
/**
* \file
* \brief contains the access buffer function.
*/

#include <string.h>
#include "ai.h"
#include "buffer.h"

/**
* \brief return statically allocated circular buffer
* \return the statically allocated ciruclar buffer
*/
buffer_t *ai_buffer_get(void)
{
	static buffer_t buf = {"", 0, 0, 0};

	if (buf.size == 0) {
		buf.size = RECV_SIZE;
		memset(buf.buf, 0, RECV_SIZE);
	}
	return (&buf);
}