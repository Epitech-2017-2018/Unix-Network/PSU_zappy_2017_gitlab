/*
** EPITECH PROJECT, 2018
** PSU_zappy_2017
** File description:
** stacks of commands responses from server
*/
/**
* \file
* \brief This file contains the function popping ressources from
* responses from server.
*/

#include "utils.h"
#include "ai.h"
#include "error.h"

/**
* \brief pop the first element added to the stack
* \return the value of the first element added to the stack
*/
char *ai_cmdlist_pop_element(void)
{
	char **head = NULL;
	char *str = NULL;
	static char tmp[MAX_RES_SIZE];

	head = ai_cmdlist(NULL);
	head = utils_wt_pop_front(head, &str);
	ai_cmdlist(head);
	memset(tmp, 0, MAX_RES_SIZE);
	if (str) {
		strcpy(tmp, str);
		free(str);
	}
	return (tmp);
}