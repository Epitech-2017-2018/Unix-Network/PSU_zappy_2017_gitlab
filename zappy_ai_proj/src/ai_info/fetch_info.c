/*
** EPITECH PROJECT, 2018
** PSU_zappy_2017
** File description:
** functions fetching info from argument
*/
/**
* \file
* \brief Those functions retrieve information from command line arguments
*/

#include "ai.h"
#include <stdlib.h>

/**
* \brief retrieve port from argument
* \param[out] main struct of the client
* \param[in] port from arguments
*/
void fetch_port(ai_info_t *info, const char *port)
{
	info->connection->port = atoi(port);
}

/**
* \brief retrieve team name from argument
* \param[out] main struct of the client
* \param[in] team name from arguments
*/
void fetch_team(ai_info_t *info, const char *team)
{
	info->team = team;
}

/**
* \brief retrieve hostname from argument
* \param[out] main struct of the client
* \param[in] hostname from arguments
*/
void fetch_hostname(ai_info_t *info, const char *hostname)
{
	info->connection->hostname = hostname;
}