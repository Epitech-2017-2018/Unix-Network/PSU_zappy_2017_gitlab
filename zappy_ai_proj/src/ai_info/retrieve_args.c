/*
** EPITECH PROJECT, 2018
** PSU_zappy_2017
** File description:
** retrieve ai arguments
*/
/**
* \file
* \brief This file contains the C check of the function.
*/

#include <ctype.h>
#include <stdio.h>
#include "ai.h"
#include "utils.h"

static int check_option_p(char const *arg)
{
	for (int i = 0; arg[i]; ++i)
		if (!isdigit(arg[i]))
			return (utils_error_reti(
				CL_INV_ARG_OPT_P_NUM, ERR_RET));
	if (atoi(arg) < 1024)
		return (utils_error_reti(
			CL_INV_ARG_PORT_VAL, ERR_RET));
	return (0);
}

static int check_options(char opt, char const *arg)
{
	if (opt == 'p')
		return (check_option_p(arg));
	if (opt == 'n') {
		if (arg[0] == '@')
			return (utils_error_reti(CL_INV_ARG_OPT_N, ERR_RET));
		return (1);
	}
	if (opt == 'h')
		return (2);
	fprintf(stderr, CL_INV_ARG_OPT, opt);
	return (ERR_RET);
}

static int analyse_options(int nb_args, char const **args, ai_info_t *info)
{
	int ret = 0;

	if (nb_args == 1) {
		fprintf(stderr, "%s\n", CL_INV_ARG_ORD);
		fprintf(stderr, CL_INV_ARG_UNFOLLOWED, *args);
		return (ERR_RET);
	}
	if (*(args[1]) == '-') {
		fprintf(stderr, "%s\n", CL_INV_ARG_ORD);
		fprintf(stderr, CL_INV_ARG_OPT_FOLLOW, *args);
		return (ERR_RET);
	}
	ret = check_options(args[0][1], args[1]);
	if (ret < 0)
		return (ERR_RET);
	fetch_client_info[ret](info, args[1]);
	return (0);
}

static int check_validity_options(int ac, char const **av, ai_info_t *info)
{
	int cont = 0;

	if (ac == 1 && strcmp(av[0], "-help") == 0)
		return (display_ai_help_message("zappy_ai"));
	if (ac == 4) {
		for (int i = 0; i < ac; ++i)
			cont = strcmp(av[i], "-h") == 0 ? cont + 1 : cont;
		if (!cont) {
			fetch_hostname(info, "localhost");
			return (1);
		}
	}
	if (ac != 6 || !av || !info)
		return (utils_error_reti(CL_INV_ARG_NB, ERR_RET));
	return (1);
}

/**
* \brief retrieve_args from command line
* \param[in] number of arguments
* \param[in] arguments
* \param[out] structure containing information about the client
* \return 0 on success. -1 on error. Message is printed on error output.
*/
int retrieve_args(int ac, char const **av, ai_info_t *info)
{
	int ret = 0;

	ret = check_validity_options(ac, av, info);
	if (ret != 1)
		return (ret);
	for (int i = 0; i < ac; ++i) {
		if (*(av[i]) == '-') {
			if (analyse_options(ac - i, av + i, info) == ERR_RET)
				return (ERR_RET);
			++i;
		}
	}
	return (0);
}