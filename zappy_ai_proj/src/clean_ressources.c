/*
** EPITECH PROJECT, 2018
** PSU_zappy_2017
** File description:
** clean ressources
*/
/**
* \file
* \brief Cleaning network mangement ressources
*/

#include <stdlib.h>
#include "ai.h"

/**
* \brief clean ressources used by the client.
* \return 0 on success.
*/
int clean_ressources(void)
{
	char **wt = ai_cmdlist(NULL);
	int sfd = saved_fd(-1);

	if (wt) {
		for (int i = 0; wt && wt[i]; ++i) {
			free(wt[i]);
			wt[i] = NULL;
		}
		free(wt);
	}
	if (sfd > 2)
		saved_fd(-2);
	return (0);
}