/*
** EPITECH PROJECT, 2018
** PSU_myirc_2017
** File description:
** Functions relative to the buffer_t struct
*/
/**
* \file
* \brief File containing function for retrieving messages from server.
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include "ai.h"
#include "utils.h"
#include "buffer.h"

/**
* \brief retrieve data from the server.
* \param[in] TCP socket server file descriptor
* \return return the number of responses added to the responses stack
*/
int get_info_from_serv(int sfd)
{
	char tmp[READ_SIZE];
	buffer_t *buf = ai_buffer_get();
	int ret = 0;
	int save = 0;

	for (int r = READ_SIZE; r == READ_SIZE; ) {
		memset(tmp, 0, READ_SIZE);
		r = recv(sfd, tmp, READ_SIZE, MSG_DONTWAIT);
		if (r == ERR_RET)
			return (utils_perror_reti("zappy: recv", -1));
		if (r >= RECV_SIZE || r == 0)
			return (utils_error_reti("zappy: invalid recv size",
			-1));
		save = ai_buffer_add_input(buf, tmp, r);
		if (save < 0)
			return (ERR_RET);
		ret += save;
	}
	return (ret);
}