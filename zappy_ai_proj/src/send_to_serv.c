/*
** EPITECH PROJECT, 2018
** Zappy
** File description:
** Main AI
*/
/**
* \file
* \brief C wrapping of sending messages to the server
*/

#include <string.h>
#include "ai.h"
#include "error.h"
#include "utils.h"

/**
* \brief send a string to the server
* \param[in] The file descriptor of the TCP server's socket
* \param[in] The string to be send
* \return The number of caracters read and send. -1 on error.
*/
int send_to_serv(char const *str)
{
	int ret = 0;
	int sfd = saved_fd(-1);
	char tmp[512];

	if (!str || sfd == -1)
		return (ERR_RET);
	ret = dprintf(sfd, "%s\n", str);
	if (ret < 0)
		return (utils_perror_reti("zappy: dprintf:", -1));
	if (ret < (int)strlen(str)) {
		memset(tmp, 0, 512);
		strcpy(tmp, str + ret);
		ret = dprintf(sfd, "%s\n", tmp);
		if (ret < 0)
			return (utils_perror_reti("zappy: dprintf:", -1));
	}
	if (ret < (int)strlen(str))
		return (utils_error_reti(ERR_CL_CMD_NOTFULLYSEND, ERR_RET));
	return (ret);
}