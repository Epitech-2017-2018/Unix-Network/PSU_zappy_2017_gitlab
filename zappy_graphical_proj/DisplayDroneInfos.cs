﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class DisplayDroneInfos : MonoBehaviour {

	private DroneInfo Info;
	private TeamsInfo Teams;
	public GameObject[] Drones;
	public GameObject Button;
	public GameObject Inventory;

	private string GetTeamAndDroneName(GameObject cur){
	string str = null;

	Teams = GameObject.Find("Client").GetComponent<TeamsInfo>();
		str = Teams.GetTeamNameById(cur.GetComponent<DroneInfo>().TeamNumber) + " - " + cur.GetComponent<DroneInfo>().Id;
		return (str);
	}

	private void RemoveExistingButton() {
		GameObject[] Buttons = GameObject.FindGameObjectsWithTag("Inventory");

		foreach (GameObject ToDestroy in Buttons) {
			Destroy(ToDestroy);
		}
	}

	private void DisplayInfos(GameObject Canvas, GameObject Drone) {
		RemoveExistingButton();
		GameObject tmp = Instantiate(Inventory, Canvas.transform);
		string str = "";

		tmp.transform.Find("Level").gameObject.GetComponentInChildren<Text>().text = str + GetComponent<DroneInfo>().Level;
		str = "x ";
		tmp.transform.Find("Life").gameObject.GetComponentInChildren<Text>().text = str + GetComponent<DroneInfo>().LifeTime;
		tmp.transform.Find("Linemate").gameObject.GetComponentInChildren<Text>().text = str + GetComponent<DroneInfo>().Linemate;
		tmp.transform.Find("Deraumere").gameObject.GetComponentInChildren<Text>().text = str + GetComponent<DroneInfo>().Deraumere;
		tmp.transform.Find("Sibur").gameObject.GetComponentInChildren<Text>().text = str + GetComponent<DroneInfo>().Sibur;
		tmp.transform.Find("Phiras").gameObject.GetComponentInChildren<Text>().text = str + GetComponent<DroneInfo>().Phiras;
		tmp.transform.Find("Mendiane").gameObject.GetComponentInChildren<Text>().text = str + GetComponent<DroneInfo>().Mendiane;
		tmp.transform.Find("Thystame").gameObject.GetComponentInChildren<Text>().text = str + GetComponent<DroneInfo>().Thystame;
	}

	private void OnMouseDown() {
		GameObject CurrentButton;
		GameObject Canvas = GameObject.Find("Canvas");
		Vector3 pos = new Vector3(-400, 350, 0);

		RemoveExistingButton();
		Info = GameObject.Find("Client").GetComponent<DroneInfo>();
		Drones = GameObject.FindGameObjectsWithTag("Player");
		foreach (GameObject Drone in Drones) {
			if (Drone.transform.position == this.transform.position) {
				CurrentButton = Instantiate(Button, Canvas.transform);
				CurrentButton.transform.localPosition = pos;
				CurrentButton.GetComponentInChildren<Text>().text = GetTeamAndDroneName(Drone);
				CurrentButton.GetComponent<Button>().onClick.AddListener(() => DisplayInfos(Canvas, Drone));
				pos.y -= 30;
			}
		}
	}
}