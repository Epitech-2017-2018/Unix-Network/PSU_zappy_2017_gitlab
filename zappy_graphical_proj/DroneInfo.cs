﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneInfo : MonoBehaviour {

	public int Level;
	public int LifeTime;
	public int Food;
	public int Linemate;
	public int Deraumere;
	public int Sibur;
	public int Phiras;
	public int Thystame;
	public int Mendiane;
	public int Id;
	public int TeamNumber;
	public Vector3[] TrianglePos = new Vector3[4];
	public Vector3[] TriangleRot = new Vector3[4];

	public void InitDirectionPosAndRot() {
		// North
		TrianglePos[0] = new Vector3(-0.5f, 0, 0);
		TriangleRot[0] = new Vector3(90, -90, 0);
		// West
		TrianglePos[1] = new Vector3(0, 0, -0.5f);
		TriangleRot[1] = new Vector3(90, -180, 0);
		// East
		TrianglePos[2] = new Vector3(0, 0, 0.5f);
		TriangleRot[2] = new Vector3(90, 0, 0);
		// South
		TrianglePos[3] = new Vector3(0.5f, 0, 0);
		TriangleRot[3] = new Vector3(90, 90, 0);
	}
}
