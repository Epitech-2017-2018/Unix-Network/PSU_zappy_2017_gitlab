﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTrantor : MonoBehaviour {

	public float speed;
	private Vector3 angles;

	private void GetInputs() {
		if (Input.GetKey(KeyCode.RightArrow)) {
			transform.Rotate (new Vector3 (0, speed * Time.deltaTime, 0));
		}
		else if (Input.GetKey(KeyCode.LeftArrow)) {
			transform.Rotate (new Vector3 (0, -speed * Time.deltaTime, 0));
		}
		//else if(Input.GetKey(KeyCode.DownArrow)) {
		//	transform.Rotate (new Vector3 (speed * Time.deltaTime, 0, 0));
		//}
		//else if(Input.GetKey(KeyCode.UpArrow)) {
		//	transform.Rotate (new Vector3 (-speed * Time.deltaTime, 0, 0));
		//}
	}

	void Update () {
		GetInputs ();
	}
}
