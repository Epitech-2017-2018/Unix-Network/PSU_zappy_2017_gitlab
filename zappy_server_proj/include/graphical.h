/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Graphical communication header.
*/
/**
* \file
* \brief Graphical protocol header
*/

#ifndef GRAPHICAL_H_
# define GRAPHICAL_H_

#include "server.h"
#include "client.h"

bool graphical_send_map(server_t *srv, client_t *client);

#endif /* !GRAPHICAL_H_ */