/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Map header
*/

#ifndef MAP_H_
# define MAP_H_

#include "zappy_server.h"

typedef struct map_tile map_tile_t;

/**
* \name direction_t
* \brief Represents the possible direction on the map
*/
typedef enum
{
	DIRECTION_NORTH,
	DIRECTION_WEST,
	DIRECTION_EAST,
	DIRECTION_SOUTH
} direction_t;

/**
* \name resource_t
* \brief Represents the different resources of the game
*/
typedef enum
{
	RESOURCE_FOOD,
	RESOURCE_LINEMATE,
	RESOURCE_DERAUMERE,
	RESOURCE_SIBUR,
	RESOURCE_MENDIANE,
	RESOURCE_PHIRAS,
	RESOURCE_THYSTAME,
	RESOURCE_END
} resource_t;

/**
* \name resource_name_table_t
* \brief Represents a member of the resource match-table
*/
typedef struct
{
	const char *name; /*!< The name of the resource*/
	resource_t resource; /*!< The resource associated to the name*/
} resource_name_table_t;

/**
* \name direction_table_t
* \brief Represents a member of the direction match-table
*/
typedef struct
{
	direction_t absolute; /*!< The absolute direction*/
	direction_t relative; /*!< The relative direction*/
	direction_t result; /*!< Result of absolute + relative combination*/
} direction_table_t;

/**
* \name map_coordinate_t
* \brief Represents a coordinate on the map
*/
typedef struct
{
	int x; /*!< X Coordinate*/
	int y; /*!< Y Coordinate*/
} map_coordinate_t;

/**
* \name map_tile_content_t
* \brief Represents the content of a tile on the map
*/
typedef struct
{
	unsigned int resources[RESOURCE_END]; /*!< The resources on the tile*/
	list_t *players; /*!< The list of players on the tile*/
	list_t *eggs; /*!< The list of eggs on the tile*/
} map_tile_content_t;

/**
* \name map_tile_t
* \brief Represents a tile on the map
*/
typedef struct map_tile
{
	map_tile_content_t content; /*!< The content of the tile*/
	map_coordinate_t coord; /*!< The coords of the tile*/
	struct map_tile *north; /*!< Pointer to the north tile*/
	struct map_tile *west; /*!< Pointer to the west tile*/
	struct map_tile *east; /*!< Pointer to the east tile*/
	struct map_tile *south; /*!< Pointer to the south tile*/
} map_tile_t;

/**
* \name map_t
* \brief Represents the map
*/
typedef struct
{
	map_coordinate_t coords; /*!< The size of the map*/
	map_tile_t *tiles; /*!< The list of tiles for the map*/
} map_t;

static const int refill_frequence = 1000;
extern const direction_table_t MAP_DIRECTION_TABLE[16];
extern const resource_name_table_t MAP_RESOURCE_NAME_TABLE[7];

bool map_init(const args_t *args, map_t *map);
void map_init_tiles(map_t *map, int max_players);
map_tile_t *map_get_tile(const map_t *map, const map_coordinate_t *coords);
map_tile_t *map_tile_by_direction(
	const map_tile_t *tile, direction_t direction);
map_tile_t *map_tile_by_relative_direction(
	const map_tile_t *tile, direction_t absolute, direction_t relative);
resource_t map_resource_by_name(const char *name);
const char *map_name_by_resource(resource_t resource);
void map_destroy(map_t *map);
bool map_refill_resources(map_t *map, resource_t resource);

#endif /* !MAP_H_ */