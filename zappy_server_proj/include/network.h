/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Network header
*/

#ifndef NETWORK_H_
# define NETWORK_H_

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "zappy_server.h"
#include "map.h"
#include "player.h"

/**
* \name host_t
* \brief Represents the network data of a host
*/
typedef struct
{
	int fd; /*!< The connection fd*/
	struct sockaddr_in addr; /*!< The host data*/
	uint16_t port; /*!< The port it is connected with*/
} host_t;

/**
* \name client_t
* \brief Represents the network-side client
*/
typedef struct
{
	host_t data; /*!< The host network data*/
	char buffer[10 * (256 + 1)]; /*!< The client circular buffer*/
	bool connected; /*!< The client status*/
	bool greeted; /*!< Set to false if not in a team or graphical*/
	bool graphical; /*!< Set to true if a client is graphical*/
	player_t player; /*!< The player associated to the client*/
} client_t;

/**
* \name player_t
* \brief Represents the game-side client
*/
typedef struct
{
	host_t data; /*!< The host network data*/
	int epfd; /*!< The epoll fd*/
	list_t *clients; /*!< The list of clients belonging to the server*/
	int frequence; /*!< The refresh rate*/
	int clientsNb; /*!< The basic number of clients per team*/
	list_t *teams; /*!< The list of teams belonging to the server*/
	map_t map; /*!< The map of the server*/
} server_t;

/**
* \name server_command_t
* \brief Represents a server command triggered by a client
*/
typedef struct
{
	const char *command; /*!< The command string*/
	int wait; /*!< The delay between the call and the execution*/
	void (*fnc)(server_t *server, client_t *client, char * const *input);
} server_commands_t;

extern const server_commands_t SERVER_COMMANDS[12];

#endif /* !NETWORK_H_ */
