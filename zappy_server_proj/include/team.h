/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Team header
*/

#ifndef TEAM_H_
# define TEAM_H_

#include "zappy_server.h"

/**
* \name team_t
* \brief Represents a team
*/
typedef struct
{
	int number; /*!< The ID of the team*/
	int hatched_eggs; /*!< The number of hatched eggs, adds a slot*/
	char *name; /*!< The name of the team*/
	list_t *members; /*!< The list of members of the team*/
} team_t;

bool team_init(team_t *team, const char *name);
bool team_add(list_t **teams, const team_t *team);
team_t *team_by_name(const list_t *teams, const char *name);
int team_count_members(const team_t *team);
bool team_exists(list_t **teams, const team_t *team);
void team_destroy(team_t *team);

#endif /* !TEAM_H_ */
