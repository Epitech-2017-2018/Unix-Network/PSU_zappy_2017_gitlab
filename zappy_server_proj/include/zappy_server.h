/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Zappy server header.
*/

#ifndef ZAPPY_SERVER_H_
# define ZAPPY_SERVER_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <ctype.h>
#include "utils.h"
#include "list.h"
#include "args.h"

#define UNUSED __attribute__((unused))
#define MOD(a, b) ((((a) % (b)) + (b)) % (b))

void setup_signals(void);
int do_zappy_server(const args_t *args);

#endif /* !ZAPPY_SERVER_H_ */
