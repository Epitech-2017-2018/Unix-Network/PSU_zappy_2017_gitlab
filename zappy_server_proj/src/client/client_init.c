/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Fills client structure
*/

#include "client.h"

/**
* \brief Fills client structure with default values
* \param[in] The client to init
*/
void client_init(client_t *client)
{
	client->connected = true;
	client->graphical = false;
	client->greeted = false;
}