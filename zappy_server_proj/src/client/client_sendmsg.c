/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Sends a message to a client
*/

#include "client.h"

/**
* \brief Sends a message to the client
* \param[in] The client
* \param[in] The message
* \return Returns false if the message failed to be sent
*/
bool client_sendmsg(client_t *client, const char *s)
{
	if (!client->connected)
		return (false);
	if (dprintf(client->data.fd, "%s\n", s) < 0) {
		perror("Err: Msg");
		client->connected = false;
		return (false);
	}
	return (true);
}