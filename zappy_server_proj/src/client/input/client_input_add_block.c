/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Add a block to the client buffer
*/

#include "client.h"

/**
* \brief Add a block to the client buffer
* \param[in] The client to write the buffer from
* \param[in] The content of the block to write
* \return Fails if the block could not be written to
*/
bool client_input_add_block(client_t *client, const char *msg)
{
	int block_idx = client_input_get_free_block(client);
	char *block;

	if (block_idx == (-1))
		return (false);
	block = client_input_get_block(client, block_idx);
	strcpy(block, msg);
	return (true);
}