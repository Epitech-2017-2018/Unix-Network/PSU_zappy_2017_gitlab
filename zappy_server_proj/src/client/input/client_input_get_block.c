/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Gets a certain block from client buffer
*/

#include "client.h"

/**
* \brief Return block X from client buffer
* \param[in] The client to read buffer from
* \param[in] The number of the block
* \return Returns the appropriate block
*/
char *client_input_get_block(const client_t *client, int block)
{
	if (block >= 10 || block < 0)
		return (NULL);
	return ((char *) &client->buffer[block * (256 + 1)]);
}