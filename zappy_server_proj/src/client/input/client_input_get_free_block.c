/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Retrieves the first free block from client buffer
*/

#include "client.h"

/**
* \brief Retrieves the first free block from client buffer
* \param[in] The client to read buffer from
* \return The free block number
*/
int client_input_get_free_block(const client_t *client)
{
	for (int it = 0; it < 10; ++it) {
		if (!client_input_get_block(client, it)[0])
			return (it);
	}
	return (-1);
}