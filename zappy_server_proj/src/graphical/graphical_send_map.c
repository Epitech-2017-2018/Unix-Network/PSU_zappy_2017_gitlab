/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Send map informations to the graphical client.
*/
/**
* \file
* \brief Send informations to the graphical client.
*/

#include "network.h"
#include "player.h"
#include "client.h"
#include "team.h"

static bool is_tile_empty(const map_tile_content_t *tile)
{
	if (tile->eggs != NULL || tile->players != NULL)
		return (false);
	for (resource_t it = 0; it < RESOURCE_END; ++it) {
		if (tile->resources[it] > 0)
			return (false);
	}
	return (true);
}

static bool send_player_info(
	client_t *client,
	player_t *player)
{
	const char *msg =
		"%d-%u:%d/%u/%u/food_%u/linemate_%u/deraumere_%u/sibur_%u/"
		"mendiane_%u/phiras_%u/thystame_%u,";

	if (dprintf(client->data.fd, msg, player->team->number, player->id,
		player->direction, player->level,
		player->inventory[RESOURCE_FOOD],
		player->inventory[RESOURCE_FOOD] / 126,
		player->inventory[RESOURCE_LINEMATE],
		player->inventory[RESOURCE_DERAUMERE],
		player->inventory[RESOURCE_SIBUR],
		player->inventory[RESOURCE_MENDIANE],
		player->inventory[RESOURCE_PHIRAS],
		player->inventory[RESOURCE_THYSTAME]) < 0)
		return (false);
	return (true);
}

static bool send_players_info(
	client_t *client,
	list_t *players)
{
	player_t *player = NULL;

	for (; players != NULL; players = players->next) {
		player = players->elm;
		if (send_player_info(client, player) == false)
			return (false);
	}
	return (true);
}

static bool send_tile_data(
	client_t *client,
	const map_tile_content_t *tile,
	int x,
	int y)
{
	const char *msg =
		"food:%u,linemate:%u,deraumere:%u,sibur:%u"
		",mendiane:%u,phiras:%u,thystame:%u|";

	if (is_tile_empty(tile))
		return (true);
	if (dprintf(client->data.fd, "%d;%d=", x, y) < 0 ||
		(tile->players != NULL && send_players_info(client,
		tile->players) == false))
		return (false);
	if (dprintf(client->data.fd, msg, tile->resources[RESOURCE_FOOD],
		tile->resources[RESOURCE_LINEMATE],
		tile->resources[RESOURCE_DERAUMERE],
		tile->resources[RESOURCE_SIBUR],
		tile->resources[RESOURCE_MENDIANE],
		tile->resources[RESOURCE_PHIRAS],
		tile->resources[RESOURCE_THYSTAME]) < 0)
		return (false);
	return (true);
}

/**
* \brief Send map with tile information to the graphical client.
* \param[in] srv The server informations.
* \param[in] client The client informations.
* \return true if everything went well, false otherwise.
*/
bool graphical_send_map(
	server_t *srv,
	client_t *client)
{
	bool res = true;
	map_tile_t *tile = srv->map.tiles;

	for (int i = 0; client->connected && i < srv->map.coords.y
		&& res; i++) {
		for (int i2 = 0; i2 < srv->map.coords.x && res; i2++) {
			tile = tile->east;
			res = send_tile_data(client, &tile->content,
				tile->coord.x, tile->coord.y);
		}
		tile = tile->south;
	}
	if (!client->connected || !res || dprintf(client->data.fd, "\n") < 0) {
		perror("Err: Msg");
		client->connected = false;
		return (false);
	}
	return (true);
}