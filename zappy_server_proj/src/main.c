/*
** EPITECH PROJECT, 2018
** Zappy
** File description:
** Main SERVER
*/

#include "zappy_server.h"
#include "team.h"

static void print_help_and_exit(const char *myself, bool error)
{
	printf("USAGE:\t%s -p port -x width -y height ", myself);
	printf("-n name1 name2 ... -c clientsNb -f freq\n\n");
	printf("\tport\t\tis the port number\n");
	printf("\twidth\t\tis the width of the world\n");
	printf("\theight\t\tis the height of the world\n");
	printf("\tnameX\t\tis the name of the team X\n");
	printf("\tclientsNb\tis the number of authorized clients per team\n");
	printf("\tfreq\t\tis the reciprocal of time unit for ");
	printf("execution of actions\n");
	exit((error) ? 84 : 0);
}

static void handle_teams(
	int argc,
	char * const *argv,
	int *optidx,
	args_t *args)
{
	team_t team;

	--(*optidx);
	for (; *optidx < argc && *argv[optind] != '-'; ++(*optidx)) {
		if (!team_init(&team, argv[*optidx]))
			continue;
		team_add(&args->teams, &team);
	}
}

static bool retrieve_arguments(int argc, char * const *argv, args_t *args)
{
	int opt;

	for (int it = 1; it < argc; ++it) {
		if (strcmp(argv[it], "-help") == 0)
		print_help_and_exit(argv[0], false);
	}
	opt = getopt(argc, argv, "p:x:y:n:c:f:");
	while (opt != (-1)) {
		switch (opt) {
		case 'p':
			args->port = atoi(optarg);
			break;
		case 'x':
			args->width = atoi(optarg);
			break;
		case 'y':
			args->height = atoi(optarg);
			break;
		case 'n':
			handle_teams(argc, argv, &optind, args);
			break;
		case 'c':
			args->clients_nb = atoi(optarg);
			break;
		case 'f':
			args->freq = atoi(optarg);
			break;
		default:
			print_help_and_exit(argv[0], true);
		}
		opt = getopt(argc, argv, "p:x:y:n:c:f:");
	}
	return (true);
}

int main(int argc, char * const *argv)
{
	args_t args = { 1111, 10, 10, NULL, 3, 100 };

	srand(getpid());
	setup_signals();
	if (!retrieve_arguments(argc, argv, &args))
		return (84);
	if (args.port <= 0 || args.width <= 0 || args.height <= 0 ||
		args.clients_nb <= 0 || args.freq <= 0) {
		fprintf(stderr, "Argument needs to be > 0\n");
		return (84);
	}
	return (do_zappy_server(&args));
}