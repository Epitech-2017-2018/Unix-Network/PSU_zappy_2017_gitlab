/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Initializes map structures
*/

#include "map.h"

const direction_table_t MAP_DIRECTION_TABLE[] =
{
	{ DIRECTION_NORTH, DIRECTION_NORTH, DIRECTION_NORTH },
	{ DIRECTION_NORTH, DIRECTION_WEST, DIRECTION_WEST },
	{ DIRECTION_NORTH, DIRECTION_EAST, DIRECTION_EAST },
	{ DIRECTION_NORTH, DIRECTION_SOUTH, DIRECTION_SOUTH },
	{ DIRECTION_WEST, DIRECTION_NORTH, DIRECTION_WEST },
	{ DIRECTION_WEST, DIRECTION_WEST, DIRECTION_SOUTH },
	{ DIRECTION_WEST, DIRECTION_EAST, DIRECTION_NORTH },
	{ DIRECTION_WEST, DIRECTION_SOUTH, DIRECTION_EAST },
	{ DIRECTION_EAST, DIRECTION_NORTH, DIRECTION_EAST },
	{ DIRECTION_EAST, DIRECTION_WEST, DIRECTION_NORTH },
	{ DIRECTION_EAST, DIRECTION_EAST, DIRECTION_SOUTH },
	{ DIRECTION_EAST, DIRECTION_SOUTH, DIRECTION_WEST },
	{ DIRECTION_SOUTH, DIRECTION_NORTH, DIRECTION_SOUTH },
	{ DIRECTION_SOUTH, DIRECTION_WEST, DIRECTION_EAST },
	{ DIRECTION_SOUTH, DIRECTION_EAST, DIRECTION_WEST },
	{ DIRECTION_SOUTH, DIRECTION_SOUTH, DIRECTION_NORTH }
};

const resource_name_table_t MAP_RESOURCE_NAME_TABLE[] =
{
	{ "food", RESOURCE_FOOD },
	{ "linemate", RESOURCE_LINEMATE },
	{ "deraumere", RESOURCE_DERAUMERE },
	{ "sibur", RESOURCE_SIBUR },
	{ "mendiane", RESOURCE_MENDIANE },
	{ "phiras", RESOURCE_PHIRAS },
	{ "thystame", RESOURCE_THYSTAME }
};

static void link_tile_vertically(
	map_tile_t *above,
	map_tile_t *curr,
	int sz)
{
	for (int it = 0; it < sz; ++it) {
		above->south = curr;
		curr->north = above;
		above = above->east;
		curr = curr->east;
	}
}

static map_tile_t *allocate_circ_row(int size, int curr_row)
{
	map_tile_t head;
	map_tile_t *curr = &head;

	memset(&head, 0, sizeof(map_tile_t));
	for (int it = 0; it < size; ++it) {
		curr->east = calloc(1, sizeof(map_tile_t));
		if (it > 0)
			curr->east->west = curr;
		if (it == (size - 1)) {
			curr->east->east = head.east;
			head.east->west = curr->east;
		}
		curr->east->coord.x = it;
		curr->east->coord.y = curr_row;
		curr = curr->east;
	}
	return (head.east);
}

/**
* \brief Initializes map structures
* \param[in] Args to get map dimensions
* \param[in] Server with its settings
* \param[out] The initialized map
* \return Fails if the map could not be alloc'd
*/
bool map_init(const args_t *args, map_t *map)
{
	map_tile_t *rows[args->height];

	map->coords.x = args->width;
	map->coords.y = args->height;
	for (int it = 0; it < map->coords.y; ++it) {
		rows[it] = allocate_circ_row(map->coords.x, it);
		if (!rows[it])
			return (false);
	}
	for (int it = 1; it < map->coords.y; ++it)
		link_tile_vertically(rows[it - 1], rows[it], map->coords.x);
	link_tile_vertically(rows[map->coords.y - 1], rows[0], map->coords.x);
	map->tiles = rows[0];
	map_init_tiles(map, args->clients_nb * list_count(args->teams));
	return (true);
}
