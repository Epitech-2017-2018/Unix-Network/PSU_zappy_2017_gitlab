/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Add resources over time.
*/

#include "network.h"
#include "map.h"

bool map_refill_resources(map_t *map, resource_t resource)
{
	map_tile_t *head = NULL;
	int tmp = rand();
	int x = tmp % 10000;
	int y = (tmp - x) < 0 ? 0 : (tmp - x) / 10000;
	map_coordinate_t coord;

	x %= map->coords.x;
	y %= map->coords.y;
	coord.x = x;
	coord.y = y;
	head = map_get_tile(map, &coord);
	head->content.resources[resource] += 1;
	return (true);
}