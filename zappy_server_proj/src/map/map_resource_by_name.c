/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Retrieve a resource according to its name
*/

#include "map.h"

/**
* \brief Retrieve a resource according to its name
* \param[in] The resource to retrieve
* \return The resource if found, otherwise RESOURCE_END
*/
resource_t map_resource_by_name(const char *name)
{
	const int sz =
		sizeof(MAP_RESOURCE_NAME_TABLE) / sizeof(resource_name_table_t);

	for (int it = 0; it < sz; ++it) {
		if (!strcmp(MAP_RESOURCE_NAME_TABLE[it].name, name))
			return (MAP_RESOURCE_NAME_TABLE[it].resource);
	}
	return (RESOURCE_END);
}