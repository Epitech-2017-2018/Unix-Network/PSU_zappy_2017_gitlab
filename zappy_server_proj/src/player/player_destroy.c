/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Destroy the player
*/

#include "player.h"

/**
* \brief Destroy the player
* \param[in] The player to destroy
*/
void player_destroy(player_t *player)
{
	list_pop_by_elm(&player->team->members, player);
	list_pop_by_elm(&player->tile->content.players, player);
	while (player->queue)
		player_pop_command(player, player->queue->elm);
}