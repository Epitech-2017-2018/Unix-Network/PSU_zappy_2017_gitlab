/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Drop a resource on the current tile
*/

#include "player.h"

/**
* \brief Drop a resource on the current tile
* \param[in] The player getting the resource
* \param[in] The resource to drop
* \return Fails if the resource is not on the player
*/
bool player_drop_resource(player_t *player, resource_t resource)
{
	if (player->inventory[resource] <= 0)
		return (false);
	if (resource == RESOURCE_FOOD)
		player->inventory[resource] -= 126;
	else
		--player->inventory[resource];
	++player->tile->content.resources[resource];
	return (true);
}