/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Init player structures, set its default values
*/

#include "player.h"

const player_elevation_table_t PLAYER_ELEVATION_TABLE[] =
{
	{ 2, 1, { 0, 1, 0, 0, 0, 0, 0 } },
	{ 3, 2, { 0, 1, 1, 1, 0, 0, 0 } },
	{ 4, 2, { 0, 2, 0, 1, 0, 2, 0 } },
	{ 5, 4, { 0, 1, 1, 2, 0, 1, 0 } },
	{ 6, 4, { 0, 1, 2, 1, 3, 0, 0 } },
	{ 7, 6, { 0, 1, 2, 3, 0, 1, 0 } },
	{ 8, 6, { 0, 2, 2, 2, 2, 2, 1 } }
};

static void player_init_inventory(player_t *player)
{
	player->inventory[RESOURCE_FOOD] = 1260;
	player->inventory[RESOURCE_LINEMATE] = 0;
	player->inventory[RESOURCE_DERAUMERE] = 0;
	player->inventory[RESOURCE_SIBUR] = 0;
	player->inventory[RESOURCE_MENDIANE] = 0;
	player->inventory[RESOURCE_PHIRAS] = 0;
	player->inventory[RESOURCE_THYSTAME] = 0;
}

/**
* \brief Init player structures, set its default values
* \param[in] The player to init
* \param[in] The player map to make the player start on
* \param[in] The team the player is assigned to
*/
bool player_init(player_t *player, map_t *map, team_t *team)
{
	static int player_id = 0;

	memset(player, 0, sizeof(player_t));
	player->tile = map_get_tile(map,
		&(map_coordinate_t) { rand() %map->coords.x,
			rand() % map->coords.y });
	if (!list_push(&player->tile->content.players, player))
		return (false);
	if (!list_push(&team->members, player)) {
		list_pop_by_elm(&team->members, player);
		return (false);
	}
	player->id = player_id++;
	player->level = 1;
	player->team = team;
	player->direction = DIRECTION_NORTH;
	player->queue = NULL;
	player_init_inventory(player);
	return (true);
}