/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Init command queue node.
*/

#include "player.h"

void player_pop_command(player_t *player, player_command_t *res)
{
	utils_wt_destroy(res->cmd);
	free(res);
	list_pop_by_elm(&player->queue, res);
}