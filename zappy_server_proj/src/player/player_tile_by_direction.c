/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Retrieves the tile according to a direction or the player direction
*/

#include "player.h"

/**
* \brief Retrieves the tile according to a direction or the player direction
* \param[in] The player to read the position from
* \param[in] The direction to get the tile from, if NULL, use player direction
* \return The tile the direction points to
*/
map_tile_t *player_tile_by_direction(
	const player_t *player,
	const direction_t *direction)
{
	map_tile_t *tile;
	const direction_t dir = (direction) ? *direction : player->direction;

	switch (dir) {
	case DIRECTION_NORTH:
		tile = player->tile->north;
		break;
	case DIRECTION_WEST:
		tile = player->tile->west;
		break;
	case DIRECTION_EAST:
		tile = player->tile->east;
		break;
	case DIRECTION_SOUTH:
		tile = player->tile->south;
		break;
	}
	return (tile);
}