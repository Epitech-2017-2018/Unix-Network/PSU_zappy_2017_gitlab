/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Retrieves the tile number according to the coordinates
*/

#include "player.h"

static map_tile_t *get_next_tile_by_id(
	const map_tile_t *tile,
	direction_t absolute,
	int id)
{
	switch (id) {
	case 1:
		return (map_tile_by_relative_direction(
			tile, absolute, DIRECTION_WEST));
	case 2:
		return (map_tile_by_relative_direction(
			tile, absolute, DIRECTION_WEST));
	case 3:
		return (map_tile_by_relative_direction(
			tile, absolute, DIRECTION_SOUTH));
	case 4:
		return (map_tile_by_relative_direction(
			tile, absolute, DIRECTION_SOUTH));
	case 5:
		return (map_tile_by_relative_direction(
			tile, absolute, DIRECTION_EAST));
	case 6:
		return (map_tile_by_relative_direction(
			tile, absolute, DIRECTION_EAST));
	case 7:
		return (map_tile_by_relative_direction(
			tile, absolute, DIRECTION_NORTH));
	case 8:
		return (map_tile_by_relative_direction(
			tile, absolute, DIRECTION_NORTH));
	}
	return (NULL);
}

/**
* \brief Retrieves the tile number according to the coordinates
* \param[in] The player to get the tiles from
* \param[in] The map coordinates to process
* \return Returns the ID of the tile
*/
int player_tile_number(
	const player_t *player,
	const map_coordinate_t *coordinates)
{
	map_tile_t *tile;

	if (player->tile->coord.x == coordinates->x &&
		player->tile->coord.y == coordinates->y)
		return (0);
	tile = map_tile_by_relative_direction(
		player->tile, player->direction, DIRECTION_NORTH);
	for (int it = 1; it <= 8; ++it) {
		if (tile->coord.x == coordinates->x &&
			tile->coord.y == coordinates->y)
			return (it);
		tile = get_next_tile_by_id(tile, player->direction, it + 1);
	}
	return (-1);
}