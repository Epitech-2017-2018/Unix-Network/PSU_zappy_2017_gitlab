/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Handle CONNECT_NBR command
*/

#include "server.h"

/**
* \brief Handle CONNECT_NBR command
* \param[in] The server
* \param[in] The client sending the command
* \param[in] The client command arguments
*/
void server_cmd_connect_nbr(
	server_t *server,
	client_t *client,
	UNUSED char * const *cmd)
{
	team_t *team = client->player.team;
	char buffer[128];

	sprintf(&buffer[0], "%d", server->clientsNb - team_count_members(team));
	client_sendmsg(client, &buffer[0]);
}