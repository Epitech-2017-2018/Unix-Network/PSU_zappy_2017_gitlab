/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Handle FORWARD command
*/

#include "server.h"

/**
* \brief Handle FORWARD command
* \param[in] The server
* \param[in] The client sending the command
* \param[in] The client command arguments
*/
void server_cmd_forward(
	UNUSED server_t *server,
	client_t *client,
	UNUSED char * const *cmd)
{
	player_t *player = &client->player;

	if (!player_move(player, player_tile_by_direction(player, NULL)))
		client_sendmsg(client, "ko");
	else
		client_sendmsg(client, "ok");
}