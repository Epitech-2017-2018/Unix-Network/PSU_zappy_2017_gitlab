/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Handle INCANTATION command
*/

#include "server.h"

static void send_ko_to_all(server_t *server, const map_tile_t *tile)
{
	client_t *client;

	for (list_t *l = tile->content.players; l; l = l->next) {
		client = server_get_client(server, l->elm);
		client_sendmsg(client, "ko");
	}
}

static const player_elevation_table_t *get_elevation(unsigned int level)
{
	const int sz = sizeof(PLAYER_ELEVATION_TABLE) /
		sizeof(player_elevation_table_t);

	for (int it = 0; it < sz; ++it) {
		if (PLAYER_ELEVATION_TABLE[it].level == level)
			return (&PLAYER_ELEVATION_TABLE[it]);
	}
	return (NULL);
}

static bool conditions_met(
	server_t *server,
	const map_tile_t *tile,
	const player_elevation_table_t *elev)
{
	unsigned int player_count = 0;

	for (list_t *curr = tile->content.players; curr; curr = curr->next) {
		if (++player_count > elev->player_nb ||
			((player_t *) (curr->elm))->level != elev->level - 1) {
			send_ko_to_all(server, tile);
			return (false);
		}
	}
	for (resource_t res = RESOURCE_FOOD + 1; res != RESOURCE_END; ++res) {
		if (tile->content.resources[res] != elev->resources[res]) {
			send_ko_to_all(server, tile);
			return (false);
		}
	}
	return (true);
}

/**
* \brief Handle INCANTATION command
* \param[in] The server
* \param[in] The client sending the command
* \param[in] The client command arguments
*/
void server_cmd_incantation(
	server_t *server,
	client_t *client,
	UNUSED char * const *cmd)
{
	char buffer[64 + 1] = { 0 };
	map_tile_t *tile = client->player.tile;
	const player_elevation_table_t *elev =
		get_elevation(client->player.level + 1);

	if (!conditions_met(server, tile, elev))
		return;
	for (resource_t res = RESOURCE_FOOD + 1; res != RESOURCE_END; ++res)
		tile->content.resources[res] -= elev->resources[res];
	for (list_t *curr = tile->content.players; curr; curr = curr->next) {
		++((player_t *) (curr->elm))->level;
		snprintf(&buffer[0], sizeof(buffer), "Current level: %d",
			((player_t *) (curr->elm))->level);
		client_sendmsg(
			server_get_client(server, curr->elm), &buffer[0]);
	}
}