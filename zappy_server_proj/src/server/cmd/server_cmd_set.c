/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Handle SET command
*/

#include "server.h"

/**
* \brief Handle SET command
* \param[in] The server
* \param[in] The client sending the command
* \param[in] The client command arguments
*/
void server_cmd_set(
	UNUSED server_t *server,
	client_t *client,
	UNUSED char * const *cmd)
{
	resource_t resource;

	if (!cmd[1]) {
		client_sendmsg(client, "ko");
		return;
	}
	resource = map_resource_by_name(cmd[1]);
	if (resource == RESOURCE_END ||
		!player_drop_resource(&client->player, resource)) {
		client_sendmsg(client, "ko");
		return;
	}
	client_sendmsg(client, "ok");
}