/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Handle TAKE command
*/

#include "server.h"

/**
* \brief Handle TAKE command
* \param[in] The server
* \param[in] The client sending the command
* \param[in] The client command arguments
*/
void server_cmd_take(
	UNUSED server_t *server,
	client_t *client,
	UNUSED char * const *cmd)
{
	resource_t resource;

	if (!cmd[1]) {
		client_sendmsg(client, "ko");
		return;
	}
	resource = map_resource_by_name(cmd[1]);
	if (resource == RESOURCE_END ||
		!player_take_resource(&client->player, resource)) {
		client_sendmsg(client, "ko");
		return;
	}
	map_refill_resources(&server->map, resource);
	client_sendmsg(client, "ok");
}