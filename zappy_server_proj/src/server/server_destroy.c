/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Destroy the server and all its subcomponents
*/

#include "server.h"

/**
* \brief Destroy the server and all its subcomponents
* \param[in] The server to destroy
*/
void server_destroy(server_t *server)
{
	for (list_t *curr = server->clients; curr; curr = server->clients) {
		server_client_disconnect(server, curr->elm);
		list_pop(&server->clients, curr);
	}
	for (list_t *curr = server->teams; curr; curr = server->teams) {
		team_destroy(curr->elm);
		free(curr->elm);
		list_pop(&server->teams, curr);
	}
	map_destroy(&server->map);
}