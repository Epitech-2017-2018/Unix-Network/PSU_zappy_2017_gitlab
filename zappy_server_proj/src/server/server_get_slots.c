/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Retrieve the number of player slots (busy or not) on the server
*/

#include "network.h"

/**
* \brief Retrieve the number of player slots (busy or not) on the server
* \param[in] The server to retrieve the slot number from
* \return The slot number for the server
*/
unsigned int server_get_slots(const server_t *server)
{
	const team_t *team;
	unsigned int slots = 0;

	for (const list_t *curr = server->teams; curr; curr = curr->next) {
		team = curr->elm;
		slots += server->clientsNb + team->hatched_eggs;
	}
	return (slots);
}
