/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Setup the signal handlers to do cleanup on proper exit
*/

#include <signal.h>
#include "zappy_server.h"
#include "server.h"

static void signal_handler(__attribute__((unused)) int sig)
{
	if (G_SERVER == NULL)
		return;
	server_destroy(G_SERVER);
	exit(0);
}

/**
* \brief Setup the signal handlers to do cleanup on proper exit
*/
void setup_signals(void)
{
	if (signal(SIGINT, &signal_handler) == SIG_ERR)
		perror("SIGINT Setup");
	if (signal(SIGTERM, &signal_handler) == SIG_ERR)
		perror("SIGINT Setup");
	if (signal(SIGQUIT, &signal_handler) == SIG_ERR)
		perror("SIGINT Setup");
}