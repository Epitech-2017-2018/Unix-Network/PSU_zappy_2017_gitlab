/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Add a team
*/

#include "team.h"

/**
* \brief Add a team to the list after error checking
* \param[in] The list of teams already present
* \param[in] The team to be added
* \return Fails if team could not be added
*/
bool team_add(list_t **teams, const team_t *team)
{
	team_t *new_team;

	if (team_exists(teams, team))
		return (utils_error_ret("Team already exists"));
	new_team = malloc(sizeof(team_t));
	if (!new_team)
		return (utils_perror_ret("team alloc"));
	memcpy(new_team, team, sizeof(team_t));
	if (!list_push(teams, new_team)) {
		fprintf(stderr, "Could not push team to list\n");
		free(new_team);
		return (false);
	}
	return (true);
}