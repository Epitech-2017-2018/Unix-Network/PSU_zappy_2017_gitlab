/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Count members from a team
*/

#include "team.h"

/**
* \brief Count members from a team
* \param[in] The team to count members from
* \return Returns the number of members found in the team
*/
int team_count_members(const team_t *team)
{
	return (list_count(team->members));
}