/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Destroy a team
*/

#include "team.h"

/**
* \brief Destroy a team
* \param[in] The team to destroy
*/
void team_destroy(team_t *team)
{
	free(team->name);
	list_destroy(&team->members, LIST_FREE_NOT, NULL);
}