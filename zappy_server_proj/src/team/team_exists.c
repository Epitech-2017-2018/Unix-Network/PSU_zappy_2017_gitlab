/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Checks if a team already exist by name of by ID
*/

#include "team.h"

/**
* \brief Checks if a team already exist by name of by ID
* \param[in] List of teams
* \param[in] The team to compare the list to
* \return Returns false if the team doesn't exist
*/
bool team_exists(list_t **teams, const team_t *team)
{
	team_t *curr_team;

	for (list_t *curr = *teams; curr; curr = curr->next) {
		curr_team = curr->elm;
		if (curr_team->number == team->number ||
			strcmp(curr_team->name, team->name) == 0)
			return (true);
	}
	return (false);
}