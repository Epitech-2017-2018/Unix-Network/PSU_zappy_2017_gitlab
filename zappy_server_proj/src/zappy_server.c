/*
** EPITECH PROJECT, 2018
** server
** File description:
** server
*/
/**
* \file
* \brief Holds the core of the server
*/

#include <unistd.h>
#include "zappy_server.h"
#include "server.h"
#include "network.h"
#include "client.h"

static bool read_client_input(client_t *client)
{
	char buf[10 * (256) + 1] = { 0 };
	char **wt;

	if (read(client->data.fd, &buf[0], 10 * 256) <= 0)
		return (false);
	wt = utils_strsplit(&buf[0], "\n", 0);
	if (!wt)
		return (false);
	for (int it = 0; wt[it]; ++it)
		client_input_add_block(client, wt[it]);
	utils_wt_destroy(wt);
	return (true);
}

static void handle_fd_io(server_t *server, struct epoll_event *ev)
{
	client_t *client;
	char *msg_block;

	if (ev->data.ptr == server) {
		server_client_accept(server);
		return;
	}
	client = ev->data.ptr;
	if (!client->connected || !read_client_input(client)) {
		server_client_disconnect(server, ev->data.ptr);
		return;
	}
	for (int it = 0; it < 10; ++it) {
		msg_block = client_input_get_block(client, it);
		if (*msg_block) {
			server_handle_input(server, client, msg_block);
			*msg_block = 0;
		}
	}
}

static bool setup_server(const args_t *args, server_t *server)
{
	struct epoll_event ev;

	if (!server_init(args, server))
		return (false);
	server->epfd = epoll_create(2048);
	if (server->epfd == (-1))
		return (utils_perror_ret("epoll_create"));
	ev.events = EPOLLIN;
	ev.data.ptr = server;
	if (epoll_ctl(
		server->epfd, EPOLL_CTL_ADD, server->data.fd, &ev) == (-1))
		return (utils_perror_ret("epoll_ctl (add)"));
	return (true);
}

static void handle_fd(server_t *server, int nfds, struct epoll_event *ev)
{
	for (int it = 0; it < nfds; ++it) {
		if (server != ev[it].data.ptr &&
			!list_get_by_elm(server->clients, ev[it].data.ptr))
			continue;
		handle_fd_io(server, &ev[it]);
	}
}

/**
* \brief Server main loop
* \param[in] Program Arguments
* \return Returns if the server initialisation succedeed
*/
int do_zappy_server(const args_t *args)
{
	int nfds;
	int sleep;
	server_t server;
	struct epoll_event events[10];

	if (!setup_server(args, memset(&server, 0, sizeof(server_t))))
		return (84);
	server_load_args(args, &server);
	sleep = 1000000 / server.frequence;
	while (!server_game_ended(&server)) {
		if (usleep(sleep) != 0)
			return (84);
		nfds = epoll_wait(server.epfd, &events[0], 10, 0);
		server_handle_time(&server);
		handle_fd(&server, nfds, &events[0]);
	}
	server_destroy(&server);
	return (0);
}